-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2016 at 04:37 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `keuangan`
--

-- --------------------------------------------------------

--
-- Table structure for table `biaya_sekolah`
--

CREATE TABLE `biaya_sekolah` (
  `biayasekolah_id` int(11) NOT NULL,
  `jenisbayar_id` int(11) NOT NULL,
  `tahunakademik_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `biayasekolah_jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biaya_sekolah`
--

INSERT INTO `biaya_sekolah` (`biayasekolah_id`, `jenisbayar_id`, `tahunakademik_id`, `kelas_id`, `biayasekolah_jumlah`) VALUES
(1, 1, 1, 1, 8000),
(2, 2, 1, 1, 5555),
(3, 3, 1, 1, 777),
(4, 4, 1, 1, 88),
(5, 5, 1, 1, 666),
(6, 6, 1, 1, 777),
(7, 7, 1, 1, 999),
(8, 8, 1, 1, 88),
(9, 9, 1, 1, 900),
(10, 10, 1, 1, 888),
(11, 11, 1, 1, 9000),
(12, 12, 1, 1, 9000),
(13, 13, 1, 1, 900),
(14, 14, 1, 1, 90000);

-- --------------------------------------------------------

--
-- Table structure for table `bukti_bayar`
--

CREATE TABLE `bukti_bayar` (
  `id_laporan` int(11) NOT NULL,
  `wali_id` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `link_photo` text NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bukti_bayar`
--

INSERT INTO `bukti_bayar` (`id_laporan`, `wali_id`, `siswa_id`, `link_photo`, `keterangan`) VALUES
(1, 1, 3, 'http://localhost/alfalahkeu/uploads/doduo-gameplay-screenshot.jpg', 'asas'),
(2, 1, 3, 'http://localhost/alfalahkeu/uploads/alvin.png', 'Uang');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'keuangan', 'Keuangan'),
(3, 'kepala sekolah', 'kepala sekolah'),
(4, 'wali', 'wali murid');

-- --------------------------------------------------------

--
-- Table structure for table `jenisbayar`
--

CREATE TABLE `jenisbayar` (
  `jenisbayar_id` int(11) NOT NULL,
  `jenisbayar_ket` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenisbayar`
--

INSERT INTO `jenisbayar` (`jenisbayar_id`, `jenisbayar_ket`) VALUES
(1, 'Uang Gedung'),
(2, 'Seragam'),
(3, 'SPP bulan 1'),
(4, 'SPP bulan 2'),
(5, 'SPP bulan 3'),
(6, 'SPP bulan 4'),
(7, 'SPP bulan 5'),
(8, 'SPP bulan 6'),
(9, 'SPP bulan 7'),
(10, 'SPP bulan 8'),
(11, 'SPP bulan 9'),
(12, 'SPP bulan 10'),
(13, 'SPP bulan 11'),
(14, 'SPP bulan 12');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `kelas_id` int(11) NOT NULL,
  `kelas_nama` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kelas_id`, `kelas_nama`) VALUES
(1, '9'),
(2, '8'),
(3, '7');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  `is_parent` int(1) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `link`, `icon`, `is_active`, `is_parent`, `level`) VALUES
(15, 'menu management', 'menu', 'fa fa-th-list', 1, 0, 1),
(18, 'data master', '#', 'fa fa-folder', 1, 0, 1),
(19, 'data kelas', 'kelas', 'fa fa-university', 1, 18, 1),
(20, 'data subkelas', 'subkelas', 'fa fa-building', 1, 18, 1),
(22, 'data tahun akademik', 'tahun_akademik', 'fa fa-calendar', 1, 18, 1),
(23, 'data siswa', 'siswa', 'fa fa-graduation-cap', 1, 18, 1),
(26, 'data jenis pembayaran', 'jenisbayar', 'fa fa-tags', 1, 18, 1),
(27, 'DATA USER', 'users', 'fa fa-users', 1, 40, 1),
(28, 'DATA HAK AKSES', 'users_groups', 'fa fa-users', 1, 40, 1),
(29, 'biaya sekolah', 'biaya_sekolah', 'fa fa-money', 1, 18, 1),
(30, 'pembayaran', 'pembayaran', 'fa fa-calculator', 1, 38, 2),
(31, 'Aduan Wali Murid', 'aduan_wali', 'fa fa-warning', 1, 0, 2),
(32, 'laporan keuangan', 'laporankeu', 'fa fa-money', 1, 39, 1),
(33, 'Persentase Pembayaran', 'persenbayar', 'fa fa-money', 1, 39, 1),
(35, 'Info siswa', 'info_siswa', 'fa fa-info-circle', 1, 0, 4),
(36, 'upload bukti bayar', 'upload_bukti', 'fa fa-upload', 1, 0, 4),
(38, 'keuangan', '#', 'fa fa-money', 1, 0, 2),
(39, 'Laporan', '#', 'fa fa-money', 1, 0, 1),
(40, 'USer Management', '#', 'fa fa-users', 1, 0, 1),
(41, 'wali siswa', 'wali_siswa', 'fa fa-users', 1, 40, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifikasi`
--

CREATE TABLE `notifikasi` (
  `notif_id` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `biayasekolah_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `pembayaran_id` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `biayasekolah_id` int(11) NOT NULL,
  `pembayaran_tanggal` date NOT NULL,
  `pembayaran_jumlah` int(11) NOT NULL,
  `sisa` int(11) NOT NULL,
  `statusbayar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`pembayaran_id`, `siswa_id`, `biayasekolah_id`, `pembayaran_tanggal`, `pembayaran_jumlah`, `sisa`, `statusbayar`) VALUES
(52, 3, 3, '2016-09-13', 777, 777, 'lunas'),
(53, 3, 4, '2016-09-13', 88, 88, 'lunas'),
(54, 3, 1, '2016-09-13', 4000, 8000, 'belum lunas'),
(55, 3, 1, '2016-09-13', 4000, 4000, 'lunas');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `siswa_id` int(11) NOT NULL,
  `siswa_nis` varchar(20) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `tahun_id` int(11) NOT NULL,
  `siswa_nama` varchar(255) NOT NULL,
  `siswa_jk` varchar(30) NOT NULL,
  `siswa_tempatlahir` varchar(255) NOT NULL,
  `siswa_tgllahir` date NOT NULL,
  `siswa_alamat` varchar(255) NOT NULL,
  `siswa_ayah` varchar(255) NOT NULL,
  `siswa_ibu` varchar(255) NOT NULL,
  `siswa_ayahkerja` varchar(255) NOT NULL,
  `siswa_ibukerja` varchar(255) NOT NULL,
  `siswa_alamatortu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`siswa_id`, `siswa_nis`, `sub_id`, `tahun_id`, `siswa_nama`, `siswa_jk`, `siswa_tempatlahir`, `siswa_tgllahir`, `siswa_alamat`, `siswa_ayah`, `siswa_ibu`, `siswa_ayahkerja`, `siswa_ibukerja`, `siswa_alamatortu`) VALUES
(1, '1234015011', 2, 2, 'Lusiyanti Roshida', 'Perempuan', 'Bojonegoro', '1993-07-05', 'Jalan Basuki Rahmat Gang Mu''in No.33a Bojonegoro', 'Eko', 'Zuhdiyah', 'Wiraswasta', 'Guru', 'Jalan Basuki Rahmat Gang Mu''in No.33a Bojonegoro'),
(3, '1234015012', 3, 1, 'Andha Yunindra R', 'Laki-Laki', 'Malang', '1990-06-01', 'surabaya', 'Hadi', 'Ayu', 'PNS', 'Guru', 'Surabaya');

-- --------------------------------------------------------

--
-- Table structure for table `subkelas`
--

CREATE TABLE `subkelas` (
  `sub_id` int(11) NOT NULL,
  `sub_nama` varchar(15) NOT NULL,
  `kelas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subkelas`
--

INSERT INTO `subkelas` (`sub_id`, `sub_nama`, `kelas_id`) VALUES
(1, 'C', 1),
(2, 'A', 1),
(3, 'A', 1),
(4, 'A', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tahun_akademik`
--

CREATE TABLE `tahun_akademik` (
  `tahun_id` int(11) NOT NULL,
  `tahun_ket` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun_akademik`
--

INSERT INTO `tahun_akademik` (`tahun_id`, `tahun_ket`) VALUES
(1, '2015'),
(2, '2016');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `phone`) VALUES
(1, 'administrator', '_iwzFP-LJAdBAsCtfF3d4gDDfZGVO_oi2TMiiCBc3MQ', 'Admin', '0'),
(2, 'rossi', '_iwzFP-LJAdBAsCtfF3d4gDDfZGVO_oi2TMiiCBc3MQ', 'rosi', '+6285655051857'),
(3, 'kepalasekolah', '_iwzFP-LJAdBAsCtfF3d4gDDfZGVO_oi2TMiiCBc3MQ', 'kepala', '09908923973'),
(4, 'walimurid1', '_iwzFP-LJAdBAsCtfF3d4gDDfZGVO_oi2TMiiCBc3MQ', 'wali', '08992932342');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(3, 2, 2),
(5, 3, 3),
(6, 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `wali_siswa`
--

CREATE TABLE `wali_siswa` (
  `wali_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wali_siswa`
--

INSERT INTO `wali_siswa` (`wali_id`, `user_id`, `siswa_id`) VALUES
(1, 4, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biaya_sekolah`
--
ALTER TABLE `biaya_sekolah`
  ADD PRIMARY KEY (`biayasekolah_id`);

--
-- Indexes for table `bukti_bayar`
--
ALTER TABLE `bukti_bayar`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenisbayar`
--
ALTER TABLE `jenisbayar`
  ADD PRIMARY KEY (`jenisbayar_id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kelas_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD PRIMARY KEY (`notif_id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`pembayaran_id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`siswa_id`);

--
-- Indexes for table `subkelas`
--
ALTER TABLE `subkelas`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `tahun_akademik`
--
ALTER TABLE `tahun_akademik`
  ADD PRIMARY KEY (`tahun_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `wali_siswa`
--
ALTER TABLE `wali_siswa`
  ADD PRIMARY KEY (`wali_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `biaya_sekolah`
--
ALTER TABLE `biaya_sekolah`
  MODIFY `biayasekolah_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `bukti_bayar`
--
ALTER TABLE `bukti_bayar`
  MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jenisbayar`
--
ALTER TABLE `jenisbayar`
  MODIFY `jenisbayar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `kelas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `notifikasi`
--
ALTER TABLE `notifikasi`
  MODIFY `notif_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `pembayaran_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `siswa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subkelas`
--
ALTER TABLE `subkelas`
  MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tahun_akademik`
--
ALTER TABLE `tahun_akademik`
  MODIFY `tahun_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `wali_siswa`
--
ALTER TABLE `wali_siswa`
  MODIFY `wali_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
