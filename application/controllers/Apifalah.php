<?php

error_reporting(0);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';

class Apifalah extends REST_Controller {

    function __construct() {
        parent::__construct();


        $this->load->model('Auth_model');
        $this->load->model('Encryption');
        $this->load->model('Pembayaran_model');
        $this->load->model('Laporankeu_model');
        $this->load->model('Info_siswa_model');

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }

// Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

            exit(0);
        }
    }

    function tes_post() {
        $tes = $this->post('tes');

        $data = $tes;


        if ($tes != "") {

            $this->response(array('tes' => $data), 200);
        } else {

            $this->response(array('tes' => $data), 500);
        }
    }

    function tes_get() {
        $tes = $this->get('tesla');

        $data = $tes;


        if ($tes != NULL) {

            $this->response(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'), 200);
        } else {

            $this->response(array('status' => false, 'data' => $data, 'msg' => 'get bangunan gagal!!'), 500);
        }
    }

    function login_post() {
        $username = $this->post("username");
        $passwordtemp = $this->post("password");
        $password = $this->Encryption->encode($passwordtemp);

        $cek = $this->Auth_model->cek_user($username, $password); //melakukan persamaan data dengan database

        if (count($cek) == 1) { //cek data berdasarkan username & pass
            $this->response(array('status' => true, 'data' => $cek, 'msg' => 'get bangunan berhasil!!'));
        } else {
            $this->response(array('status' => false, 'data' => $cek, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function infosiswa_post() {
        $user_id = $this->post("user_id");
        $siswa_wali = $this->Info_siswa_model->getSiswawali($user_id);

        if ($siswa_wali != NULL) {
            $this->response(array('status' => true, 'data' => $siswa_wali, 'msg' => 'get bangunan berhasil!!'));
        } else {
            $this->response(array('status' => false, 'data' => $siswa_wali, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function infobayar_post() {
        $user_id = $this->post("user_id");
        $siswa_wali = $this->Info_siswa_model->getSiswawali($user_id);
        foreach ($siswa_wali as $a) {
            $siswa_id = $a->siswa_id;
        }
        $pembayaransiswa = $this->Pembayaran_model->getPembayaransiswa($siswa_id);


        if ($siswa_wali != NULL) {
            $this->response(array('status' => true, 'data' => $pembayaransiswa, 'msg' => 'get bangunan berhasil!!'));
        } else {
            $this->response(array('status' => false, 'data' => $pembayaransiswa, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function laporhari_post() {
        $tanggalawal = $this->post('tanggalawal');
        $tanggalakhir = $this->post('tanggalakhir');

        $data = $this->Laporankeu_model->persenPerhari($tanggalawal, $tanggalakhir);


        if ($data != NULL) {
            $this->response(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'));
        } else {
            $this->response(array('status' => false, 'data' => $data, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function laporbulan_post() {
        $bulanawal = $this->post('bulanawal');
        $bulanakhir = $this->post('bulanakhir');

        $data = $this->Laporankeu_model->persenPerbulan($bulanawal, $bulanakhir);

        if ($data != NULL) {
            $this->response(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'));
        } else {
            $this->response(array('status' => false, 'data' => $data, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function laportahun_post() {
        $tahunawal = $this->post('tahunawal');
        $tahunakhir = $this->post('tahunakhir');

        $data = $this->Laporankeu_model->persenPertahun($tahunawal, $tahunakhir);
        if ($data != NULL) {
            $this->response(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'));
        } else {
            $this->response(array('status' => false, 'data' => $data, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function getnotif_post() {
        $siswa_id = $this->post('siswa_id');
        $jumlah_notif = $this->Pembayaran_model->countnotif($siswa_id);
        $notif_list = $this->Pembayaran_model->getnotif($siswa_id);
        if ($jumlah_notif != NULL) {
            $this->response(array('status' => true, 'jumlah_notif' => $jumlah_notif, 'notif_list '=>$notif_list,'msg' => 'get notif berhasil!!'));
        } else {
            $this->response(array('status' => false, 'jumlah_notif' => $jumlah_notif, 'notif_list '=>$notif_list, 'msg' => 'get notif gagal!!'));
        }
    }

    function deletenotif_post() {
        $siswa_id = $this->post('siswa_id');
        $delete = $this->Pembayaran_model->deletenotif($siswa_id);
        if ($delete == TRUE) {
            $this->response(array('status' => true,  'msg' => 'delete berhasil!!'));
        } else {
            $this->response(array('status' => false,  'msg' => 'delete gagal!!'));
        }
    }

    function upload_buktibayar_post() {
        $config['upload_path'] = './uploads/';
        $config['overwrite'] = TRUE;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2048';
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['remove_spaces'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
//            $error = array('error' => $this->upload->display_errors());
//

            $this->response(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'));
        } else {
            $upload_data = $this->upload->data();
            $gambar = $upload_data['file_name'];

            $link = 'http://' . $_SERVER['SERVER_NAME'] . '/alfalahkeu/uploads/' . $gambar;
            $data = array(
                'wali_id' => $this->post('wali_id', TRUE),
                'siswa_id' => $this->post('siswa_id', TRUE),
                'link_photo' => $link,
                'keterangan' => $this->post('keterangan', TRUE)
            );

            // model
            $data = $this->Upload_bukti_model->insert($data);
            if ($data != NULL) {
                $this->response(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'));
            } else {
                $this->response(array('status' => false, 'data' => $data, 'msg' => 'get bangunan gagal!!'));
            }
        }
    }

}

?>