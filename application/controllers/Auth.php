<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('auth');
        $this->load->helper('form', 'url');
        $this->load->model('Auth_model');
        $this->load->model('Encryption');
    }

    function index() {
        $session = $this->session->userdata('isLogin'); //mengabil dari session apakah sudah login atau belum
        if ($session == FALSE) { //jika session false maka akan menampilkan halaman login
            $this->load->view('login');
        } else { //jika session true maka di redirect ke halaman dashboard
            redirect('auth');
        }
    }

    function do_login() {
        $username = $this->input->post("username");
        $passwordtemp = $this->input->post("password");



        $password = $this->Encryption->encode($passwordtemp);

        $cek = $this->Auth_model->cek_user($username, $password); //melakukan persamaan data dengan database


       

        if (count($cek) == 1) { //cek data berdasarkan username & pass
            foreach ($cek as $cek1) {
                $level = $cek1['group_id']; //mengambil data(level/hak akses) dari database
                $user_id = $cek1['user_id'];
            }

            $this->session->set_userdata(array(
                'isLogin' => TRUE, //set data telah login
                'username' => $username, //set session username
                'lvl' => $level, //set session hak akses
                'user_id' => $user_id
            ));

            redirect('dashboard', 'refresh');  //redirect ke halaman dashboard
        } else { //jika data tidak ada yng sama dengan database
            echo "<script>alert('Gagal Login!')</script>";
            redirect('auth', 'refresh');
        }
    }

}
