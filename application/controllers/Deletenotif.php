<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Deletenotif extends CI_Controller {

    function __construct() {
        parent::__construct();
       
        $this->load->model('Pembayaran_model');
        $this->load->model('Info_siswa_model');
       
    }

    function delete (){
         $user_id = $this->session->userdata('user_id');
            $siswa_wali = $this->Info_siswa_model->getSiswawali($user_id);

            foreach ($siswa_wali as $a) {
                $siswa_id = $a->siswa_id;
            }
            $this->Pembayaran_model->deletenotif($siswa_id);
      
            redirect('info_siswa');
    }
    

}
