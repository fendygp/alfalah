<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wali_siswa extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Wali_siswa_model');

        $this->load->library('form_validation');
    }

    public function index() {
        $wali_siswa = $this->Wali_siswa_model->get_all();

        $data = array(
            'wali_siswa_data' => $wali_siswa
        );

        $this->template->load('template', 'wali_siswa_list', $data);
    }

    public function read($id) {
        $row = $this->Wali_siswa_model->get_by_id($id);
        if ($row) {
            $data = array(
                'wali_id' => $row->wali_id,
                'user_id' => $row->username,
                'siswa_id' => $row->siswa_nama,
            );
            $this->template->load('template', 'wali_siswa_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wali_siswa'));
        }
    }

    public function create() {
        $siswa = $this->Wali_siswa_model->getAllsiswa();
        $user = $this->Wali_siswa_model->getAlluser();
        $data = array(
            'button' => 'Create',
            'action' => site_url('wali_siswa/create_action'),
            'wali_id' => set_value('wali_id'),
            'user_id' => set_value('user_id'),
            'siswa_id' => set_value('siswa_id'),
            'siswa_data' => $siswa,
            'user_data' => $user
        );
        $this->template->load('template', 'wali_siswa_form', $data);
    }

    public function create_action() {
//        $this->_rules();
//
//        if ($this->form_validation->run() == FALSE) {
//            $this->create();
//        } else {
            $data = array(
                'user_id' => $this->input->post('user_id', TRUE),
                'siswa_id' => $this->input->post('siswa_id', TRUE),
            );

            $this->Wali_siswa_model->insert($data);
//            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('wali_siswa'));
//        }
    }

    public function update($id) {
        $row = $this->Wali_siswa_model->get_by_id($id);
        $siswa = $this->Wali_siswa_model->getAllsiswa();
        $user = $this->Wali_siswa_model->getAlluser();

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('wali_siswa/update_action'),
                'wali_id' => set_value('wali_id', $row->wali_id),
                'user_id' => set_value('user_id', $row->user_id),
                'siswa_id' => set_value('siswa_id', $row->siswa_id),
                'siswa_data' => $siswa,
                'user_data' => $user
            );
            $this->template->load('template', 'wali_siswa_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wali_siswa'));
        }
    }

    public function update_action() {
//        $this->_rules();
//
//        if ($this->form_validation->run() == FALSE) {
//            $this->update($this->input->post('wali_id', TRUE));
//        } else {
            $data = array(
                'user_id' => $this->input->post('user_id', TRUE),
                'siswa_id' => $this->input->post('siswa_id', TRUE),
            );

            $this->Wali_siswa_model->update($this->input->post('wali_id', TRUE), $data);
//            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('wali_siswa'));
//        }
    }

    public function delete($id) {
        $row = $this->Wali_siswa_model->get_by_id($id);

        if ($row) {
            $this->Wali_siswa_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('wali_siswa'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('wali_siswa'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('user_id', 'user id', 'trim|required');
        $this->form_validation->set_rules('siswa_id', 'siswa id', 'trim|required');

        $this->form_validation->set_rules('wali_id', 'wali_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "wali_siswa.xls";
        $judul = "wali_siswa";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "User Id");
        xlsWriteLabel($tablehead, $kolomhead++, "Siswa Id");

        foreach ($this->Wali_siswa_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->user_id);
            xlsWriteNumber($tablebody, $kolombody++, $data->siswa_id);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=wali_siswa.doc");

        $data = array(
            'wali_siswa_data' => $this->Wali_siswa_model->get_all(),
            'start' => 0
        );

        $this->load->view('wali_siswa_doc', $data);
    }

}

/* End of file Wali_siswa.php */
/* Location: ./application/controllers/Wali_siswa.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-15 11:08:46 */
/* http://harviacode.com */