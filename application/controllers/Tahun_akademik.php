<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tahun_akademik extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Tahun_akademik_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $tahun_akademik = $this->Tahun_akademik_model->get_all();

        $data = array(
            'tahun_akademik_data' => $tahun_akademik
        );

        $this->template->load('template', 'tahun_akademik_list', $data);
    }

    public function read($id) {
        $row = $this->Tahun_akademik_model->get_by_id($id);
        if ($row) {
            $data = array(
                'tahun_id' => $row->tahun_id,
                'tahun_ket' => $row->tahun_ket,
                'status' => $row->status,
            );
            $this->template->load('template', 'tahun_akademik_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tahun_akademik'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tahun_akademik/create_action'),
            'tahun_id' => set_value('tahun_id'),
            'tahun_ket' => set_value('tahun_ket'),
            'status' => set_value('status'),
        );
        $this->template->load('template', 'tahun_akademik_form', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'tahun_ket' => $this->input->post('tahun_ket', TRUE),
                'status' => $this->input->post('status', TRUE),
            );

            $this->Tahun_akademik_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tahun_akademik'));
        }
    }

    public function update($id) {
        $row = $this->Tahun_akademik_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tahun_akademik/update_action'),
                'tahun_id' => set_value('tahun_id', $row->tahun_id),
                'tahun_ket' => set_value('tahun_ket', $row->tahun_ket),
                'status' => set_value('status', $row->status),
            );
            $this->template->load('template', 'tahun_akademik_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tahun_akademik'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('tahun_id', TRUE));
        } else {
            $data = array(
                'tahun_ket' => $this->input->post('tahun_ket', TRUE),
                'status' => $this->input->post('status', TRUE),
            );

            $this->Tahun_akademik_model->update($this->input->post('tahun_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tahun_akademik'));
        }
    }

    public function delete($id) {
        $row = $this->Tahun_akademik_model->get_by_id($id);

        if ($row) {
            $this->Tahun_akademik_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tahun_akademik'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tahun_akademik'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('tahun_ket', 'tahun ket', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');


        $this->form_validation->set_rules('tahun_id', 'tahun_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "tahun_akademik.xls";
        $judul = "tahun_akademik";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Tahun Ket");
        xlsWriteLabel($tablehead, $kolomhead++, "Statust");

        foreach ($this->Tahun_akademik_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->tahun_ket);
            xlsWriteLabel($tablebody, $kolombody++, $data->status);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=tahun_akademik.doc");

        $data = array(
            'tahun_akademik_data' => $this->Tahun_akademik_model->get_all(),
            'start' => 0
        );

        $this->load->view('tahun_akademik_doc', $data);
    }

    function pdf() {
        $data = array(
            'tahun_akademik_data' => $this->Tahun_akademik_model->get_all(),
            'start' => 0
        );

        $this->load->library('pdf');


        $html = $this->load->view('tahun_akademik_doc', $data, true);

        $this->pdf->generate($html, 'tahun_akademik');
    }

}

/* End of file Tahun_akademik.php */
/* Location: ./application/controllers/Tahun_akademik.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-06-28 08:43:54 */
/* http://harviacode.com */