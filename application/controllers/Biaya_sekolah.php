<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Biaya_sekolah extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Biaya_sekolah_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $biaya_sekolah = $this->Biaya_sekolah_model->get_all();
        $tahunakademik = $this->Biaya_sekolah_model->getTahunAkademik();

        $data = array(
            'action' => site_url('biaya_sekolah/get_kelas/'),
            'biaya_sekolah_data' => $biaya_sekolah,
            'tahun_akademik_data' => $tahunakademik
        );

        $this->template->load('template', 'biaya_sekolah_list', $data);
    }

    public function read($id) {
        $row = $this->Biaya_sekolah_model->get_by_id($id);
        if ($row) {
            $data = array(
                'biayasekolah_id' => $row->biayasekolah_id,
                'jenisbayar_id' => $row->jenisbayar_id,
                'kelas_id' => $row->kelas_id,
                'tahunakademik_id' => $row->tahunakademik_id,
                'biayasekolah_jumlah' => $row->biayasekolah_jumlah,
            );
            $this->template->load('template', 'biaya_sekolah_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('biaya_sekolah'));
        }
    }

    public function update($id) {
        $row = $this->Biaya_sekolah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('biaya_sekolah/update_action'),
                'biayasekolah_id' => set_value('biayasekolah_id', $row->biayasekolah_id),
                'jenisbayar_id' => set_value('jenisbayar_id', $row->jenisbayar_id),
                'tahunakademik_id' => set_value('tahunakademik_id', $row->tahunakademik_id),
                'biayasekolah_jumlah' => set_value('biayasekolah_jumlah', $row->biayasekolah_jumlah),
                'tahunakademik_ket' => set_value('tahunakademik_ket', $row->tahun_ket),
                'jenisbayar_ket' => set_value('jenisbayar_ket', $row->jenisbayar_ket),
            );
            $this->template->load('template', 'biaya_sekolah_form', $data);
        } else {
             echo "<script>alert('update gagal!')</script>";
            redirect(site_url('biaya_sekolah'));
        }
    }

    public function update_action() {
  
            $data = array(
                'jenisbayar_id' => $this->input->post('jenisbayar_id', TRUE),
                'tahunakademik_id' => $this->input->post('tahunakademik_id', TRUE),
                'biayasekolah_jumlah' => $this->input->post('biayasekolah_jumlah', TRUE),
            );
            
            

            $this->Biaya_sekolah_model->update($this->input->post('biayasekolah_id', TRUE), $data);
            echo "<script>alert('update berhasil!')</script>";
            redirect(site_url('biaya_sekolah'));
        
    }

    public function delete($id) {
        $row = $this->Biaya_sekolah_model->get_by_id($id);

        if ($row) {
            $this->Biaya_sekolah_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('biaya_sekolah'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('biaya_sekolah'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('jenisbayar_id', 'jenisbayar id', 'trim|required');
        $this->form_validation->set_rules('tahun_id', 'tahun id', 'trim|required');
        $this->form_validation->set_rules('kelas_id', 'kelas id', 'trim|required');
        $this->form_validation->set_rules('biayasekolah_jumlah', 'biayasekolah jumlah', 'trim|required');

        $this->form_validation->set_rules('biayasekolah_id', 'biayasekolah_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "biaya_sekolah.xls";
        $judul = "biaya_sekolah";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Jenisbayar Id");
        xlsWriteLabel($tablehead, $kolomhead++, "Tahunakademik Id");
        xlsWriteLabel($tablehead, $kolomhead++, "Biayasekolah Jumlah");

        foreach ($this->Biaya_sekolah_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->jenisbayar_id);
            xlsWriteNumber($tablebody, $kolombody++, $data->tahunakademik_id);
            xlsWriteNumber($tablebody, $kolombody++, $data->biayasekolah_jumlah);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=biaya_sekolah.doc");

        $data = array(
            'biaya_sekolah_data' => $this->Biaya_sekolah_model->get_all(),
            'start' => 0
        );

        $this->load->view('biaya_sekolah_doc', $data);
    }

    function pdf() {
        $data = array(
            'biaya_sekolah_data' => $this->Biaya_sekolah_model->get_all(),
            'start' => 0
        );


        $this->load->library('pdf');


        $html = $this->load->view('biaya_sekolah_doc', $data, true);

        $this->pdf->generate($html, 'biaya_sekolah');
        ;
    }

    function get_kelas() {
        $tahun_id = $this->input->post('tahun_id');
        $tahun_ket = $this->Biaya_sekolah_model->getTahunAkademikbyId($tahun_id);

        $kelas = $this->Biaya_sekolah_model->getKelas();
        $biaya_sekolah = $this->Biaya_sekolah_model->get_all();

        $data = array(
            'action' => site_url('biaya_sekolah/inputbiaya/'),
            'tahun_id' => $tahun_id,
            'tahun_ket' => $tahun_ket,
            'biaya_sekolah_data' => $biaya_sekolah,
            'kelas_data' => $kelas
        );

        $this->template->load('template', 'biaya_sekolah_list2', $data);
    }

    function inputbiaya() {
        $tahun_id = $this->input->post('tahun_id');
        $kelas_id = $this->input->post('kelas_id');
        $tahun_ket = $this->Biaya_sekolah_model->getTahunAkademikbyId($tahun_id);
        $jenisbayar = $this->Biaya_sekolah_model->getJenisbayar();
        $kelas = $this->Biaya_sekolah_model->getKelasbyId($kelas_id);


        $cek = $this->Biaya_sekolah_model->getBiayabyThnKls($tahun_id, $kelas_id);


        if ($cek == TRUE) {

            $data = array(
                'action' => site_url('biaya_sekolah/prosesupdatebiaya/'),
                'jenisbayar_data' => $jenisbayar,
                'tahun_id' => $tahun_id,
                'tahun_ket' => $tahun_ket,
                'biaya_sekolah_data' => $cek,
                'kelas_id' => $kelas_id,
                'kelas_data' => $kelas
            );
            $this->template->load('template', 'biaya_sekolah_list4', $data);
        } else {

            $data = array(
                'action' => site_url('biaya_sekolah/prosesinsertbiaya/'),
                'jenisbayar_data' => $jenisbayar,
                'tahun_id' => $tahun_id,
                'tahun_ket' => $tahun_ket,
                'biaya_sekolah_data' => $cek,
                'kelas_id' => $kelas_id,
                'kelas_data' => $kelas
            );
            $this->template->load('template', 'biaya_sekolah_list3', $data);
        }
    }

    function prosesinsertbiaya() {

        $jenisbayar = $this->Biaya_sekolah_model->getJenisbayar();

        foreach ($jenisbayar as $a) {
            $data = array(
                'tahunakademik_id' => $this->input->post('tahun_id_' . $a->jenisbayar_id, TRUE),
                'kelas_id' => $this->input->post('kelas_id_' . $a->jenisbayar_id, TRUE),
                'jenisbayar_id' => $this->input->post('jenisbayar_id_' . $a->jenisbayar_id, TRUE),
                'biayasekolah_jumlah' => $this->input->post('jumlah_bayar_' . $a->jenisbayar_id, TRUE)
            );

            $this->Biaya_sekolah_model->insert($data);
        }

        redirect(site_url('biaya_sekolah'));
    }

    function prosesupdatebiaya() {

        $jenisbayar = $this->Biaya_sekolah_model->getJenisbayar();

        foreach ($jenisbayar as $a) {
            $data = array(
                'tahunakademik_id' => $this->input->post('tahun_id_' . $a->jenisbayar_id, TRUE),
                'kelas_id' => $this->input->post('kelas_id_' . $a->jenisbayar_id, TRUE),
                'jenisbayar_id' => $this->input->post('jenisbayar_id_' . $a->jenisbayar_id, TRUE),
                'biayasekolah_jumlah' => $this->input->post('jumlah_bayar_' . $a->jenisbayar_id, TRUE),
            );

            $id = $this->input->post('biayasekolah_id_' . $a->jenisbayar_id);

            echo $id;
            $this->Biaya_sekolah_model->update($id, $data);
        }

        redirect(site_url('biaya_sekolah'));
    }

}

/* End of file Biaya_sekolah.php */
/* Location: ./application/controllers/Biaya_sekolah.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-07-18 23:31:46 */
/* http://harviacode.com */
