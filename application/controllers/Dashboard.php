<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Pembayaran_model');
        $this->load->model('Info_siswa_model');
        $this->load->library('auth');
        $this->auth->cek_auth(); //ngambil auth dari library
        if (empty($this->session->userdata("username")))
            redirect(site_url(), 'refresh');
    }

    function index() {
        //	$ambil_akun = $this->Auth_model->ambil_user($this->session->userdata('username'));
        //	$ambil_level = $this->Auth_model->ambil_menu($this->session->userdata('lvl'));
        //	$data['user'] = $ambil_akun;
        $data['menulevel'] = $this->session->userdata('lvl');

        if ($data['menulevel'] == 1) {//admin
            //	$data['menulevel']= $this->session->userdata('lvl');
            $this->template->load('template', 'dashboard', $data);
            //	$this->load->view('template/head');
            //	$this->load->view('template/topbar');
//$this->load->view('template/sidebar');
            //	$this->load->view('dashboard_admin',$data);
            //	$this->load->view('template/foot');
        } elseif ($data['menulevel'] == 2) { //user
            $this->template->load('template', 'dashboard', $data);
            //$data['menulevel']=$this->session->userdata('lvl');
            //$this->load->view('template/head');
            //$this->load->view('template/topbar');
            //$this->load->view('template/sidebar',$data);
            //$this->load->view('dashboard_member',$data);
            //$this->load->view('template/foot');
        } elseif ($data['menulevel'] == 3) { //user
            $this->template->load('template', 'dashboard', $data);
            //$data['menulevel']=$this->session->userdata('lvl');
            //$this->load->view('template/head');
            //$this->load->view('template/topbar');
            //$this->load->view('template/sidebar',$data);
            //$this->load->view('dashboard_member',$data);
            //$this->load->view('template/foot');
        } elseif ($data['menulevel'] == 4) { //user
            $user_id = $this->session->userdata('user_id');
            $siswa_wali = $this->Info_siswa_model->getSiswawali($user_id);

            foreach ($siswa_wali as $a) {
                $siswa_id = $a->siswa_id;
            }
            $totalnotif = $this->Pembayaran_model->countnotif($siswa_id);
            $getnotif = $this->Pembayaran_model->getnotif($siswa_id);

            $data = array(
                'user_id' => $user_id,
                'siswa_data' => $siswa_wali,
                'totalnotif' => $totalnotif,
                'getnotif' => $getnotif
            );
            $this->template->load('template', 'dashboard', $data);
            //$data['menulevel']=$this->session->userdata('lvl');
            //$this->load->view('template/head');
            //$this->load->view('template/topbar');
            //$this->load->view('template/sidebar',$data);
            //$this->load->view('dashboard_member',$data);
            //$this->load->view('template/foot');
        }
    }
    
       
    function login() {
        $session = $this->session->userdata('isLogin');
        if ($session == FALSE) {
            $this->load->view('login');
        } else {
            redirect('dashboard');
        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('auth', 'refresh');
    }

}
