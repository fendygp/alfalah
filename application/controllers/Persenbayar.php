<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Persenbayar extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Laporankeu_model');
        $this->load->library('form_validation');
    }

    function index() {

        $data = array(
            'action' => site_url('Persenbayar/pilihjenispersen'),
        );
        $this->template->load('template', 'jenispersen_form', $data);
    }

//    function pilihjenispersen2() {
//        $jenislapor = $this->input->post('jenis_persen');
//
//        if ($jenislapor == "harian") {
//            $lapordata = $this->Laporankeu_model->laporPerhari();
//            $data = array(
//                'lapordata' => $lapordata,
//                'action' => site_url('Persenbayar/pilihjenispersen2')
//            );
//            $this->template->load('template', 'jenispersen_harilist2', $data);
//        } elseif ($jenislapor == "bulanan") {
//            $lapordata = $this->Laporankeu_model->laporPerbulan();
//            $data = array(
//                'lapordata' => $lapordata,
//                'action' => site_url('Persenbayar/pilihjenispersen2')
//            );
//            $this->template->load('template', 'jenispersen_bulanlist2', $data);
//        } elseif ($jenislapor == "tahunan") {
//            $lapordata = $this->Laporankeu_model->laporPertahun();
//            $data = array(
//                'lapordata' => $lapordata,
//                'action' => site_url('Persenbayar/pilihjenispersen2')
//            );
//            $this->template->load('template', 'jenispersen_tahunlist2', $data);
//        }
//    }

    function pilihjenispersen() {
        $jenislapor = $this->input->post('jenis_persen');

        if ($jenislapor == "harian") {
            $data = array(
                'action' => site_url('Persenbayar/harianlist')
            );
            $this->template->load('template', 'jenispersen_hari', $data);
        } elseif ($jenislapor == "bulanan") {

            $data = array(
                'action' => site_url('Persenbayar/bulananlist')
            );
            $this->template->load('template', 'jenispersen_bulan', $data);
        } elseif ($jenislapor == "tahunan") {
            $tahunakademik = $this->Laporankeu_model->getTahunAkademik();
            $data = array(
                'tahundata' => $tahunakademik,
                'action' => site_url('Persenbayar/tahunanlist')
            );
            $this->template->load('template', 'jenispersen_tahun', $data);
        }
    }

    function harianlist() {
        $tanggalawal = $this->input->post('tanggalawal');
        $tanggalakhir = $this->input->post('tanggalakhir');

        $lapordata = $this->Laporankeu_model->persenPerhari($tanggalawal, $tanggalakhir);
       
        
        $data = array(
            'lapordata' => $lapordata,
            'action' => site_url('Persenbayar/harianlist'),
            'tanggalawal' => $tanggalawal,
            'tanggalakhir' => $tanggalakhir
        );
        $this->template->load('template', 'jenispersen_harilist2', $data);

//        $hariandata= $this->Laporankeu_model->persenPerhari($tanggalawal, $tanggalakhir);
//        $kreditdata = $this->Laporankeu_model->jumlahKredit();
//
//        foreach ($hariandata as $harian) {
//
//            if ($harian->totalmasuk == NULL) {
//                $totalmasuk = 0;
//            } else {
//                $totalmasuk = $harian->totalmasuk;
//            }
//        }
//
//        foreach ($kreditdata as $kredit) {
//            $totalkredit = $kredit->totalkredit;
//        }
//        $belumdibayar = $totalkredit - $totalmasuk;
//
//        $data = array(
//            'action' => site_url('Persenbayar/harianlist'),
//            'totalmasuk' => $totalmasuk,
//            'belumdibayar' => $belumdibayar,
//            'totalkredit' => $totalkredit,
//            'tanggalawal' => $tanggalawal,
//            'tanggalakhir' => $tanggalakhir
//        );
//        $this->template->load('template', 'jenispersen_harilist', $data);
    }

    function bulananlist() {
        $bulanawal = $this->input->post('bulanawal');
        $bulanakhir = $this->input->post('bulanakhir');

        $lapordata = $this->Laporankeu_model->persenPerbulan($bulanawal, $bulanakhir);
        $data = array(
            'bulanawal' => $bulanawal,
            'bulanakhir' => $bulanakhir,
            'lapordata' => $lapordata,
            'action' => site_url('Persenbayar/bulananlist')
        );
        $this->template->load('template', 'jenispersen_bulanlist2', $data);


//        $bulan = $this->input->post('bulan');
//        $hariandata = $this->Laporankeu_model->persenPerbulan($bulan);
//        $kreditdata = $this->Laporankeu_model->jumlahKredit();
//
//        foreach ($hariandata as $harian) {
//
//            if ($harian->totalmasuk == NULL) {
//                $totalmasuk = 0;
//            } else {
//                $totalmasuk = $harian->totalmasuk;
//            }
//        }
//
//        foreach ($kreditdata as $kredit) {
//            $totalkredit = $kredit->totalkredit;
//        }
//        $belumdibayar = $totalkredit - $totalmasuk;
//
//        $data = array(
//            'action' => site_url('Persenbayar/bulananlist'),
//            'totalmasuk' => $totalmasuk,
//            'belumdibayar' => $belumdibayar,
//            'totalkredit' => $totalkredit,
//            'bulan' => $bulan
//        );
//
//        $this->template->load('template', 'jenispersen_bulanlist', $data);
    }

    function tahunanlist() {
        $tahunawal = $this->input->post('tahunawal');
        $tahunakhir = $this->input->post('tahunakhir');
        $tahunakademik = $this->Laporankeu_model->getTahunAkademik();
        $lapordata = $this->Laporankeu_model->persenPertahun($tahunawal, $tahunakhir);
        $data = array(
            'lapordata' => $lapordata,
            'tahunawal' => $tahunawal,
            'tahunakhir' => $tahunakhir,
            'tahundata' => $tahunakademik,
            'action' => site_url('Persenbayar/tahunanlist')
        );
        $this->template->load('template', 'jenispersen_tahunlist2', $data);



//        $tahun = $this->input->post('tahun');
//        $tahunakademik = $this->Laporankeu_model->getTahunAkademik();
//        $hariandata = $this->Laporankeu_model->persenPertahun($tahun);
//        $kreditdata = $this->Laporankeu_model->jumlahKredit();
//
//        foreach ($hariandata as $harian) {
//
//            if ($harian->totalmasuk == NULL) {
//                $totalmasuk = 0;
//            } else {
//                $totalmasuk = $harian->totalmasuk;
//            }
//        }
//
//        foreach ($kreditdata as $kredit) {
//            $totalkredit = $kredit->totalkredit;
//        }
//        $belumdibayar = $totalkredit - $totalmasuk;
//
//        $data = array(
//            'action' => site_url('Persenbayar/tahunanlist'),
//            'totalmasuk' => $totalmasuk,
//            'belumdibayar' => $belumdibayar,
//            'totalkredit' => $totalkredit,
//            'tahundata' => $tahunakademik,
//            'tahun' => $tahun
//        );
//
//        $this->template->load('template', 'jenispersen_tahunlist', $data);
    }

}

/* End of file Pembayaran.php */
/* Location: ./application/controllers/Pembayaran.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-07-22 05:30:14 */
/* http://harviacode.com */