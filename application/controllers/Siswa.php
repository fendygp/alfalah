<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siswa extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Siswa_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $siswa = $this->Siswa_model->get_all_query();

        $data = array(
            'siswa_data' => $siswa
        );

        $this->template->load('template', 'siswa_list', $data);
    }

    public function read($id) {
        $row = $this->Siswa_model->get_by_id($id);
        if ($row) {
            $data = array(
                'siswa_id' => $row->siswa_id,
                'siswa_nis' => $row->siswa_nis,
                'sub_id' => $row->sub_id,
                'kelas_nama' => $row->kelas_nama,
                'sub_nama' => $row->sub_nama,
                'tahun_id' => $row->tahun_ket,
                'siswa_nama' => $row->siswa_nama,
                'siswa_jk' => $row->siswa_jk,
                'siswa_tempatlahir' => $row->siswa_tempatlahir,
                'siswa_tgllahir' => $row->siswa_tgllahir,
                'siswa_alamat' => $row->siswa_alamat,
                'siswa_ayah' => $row->siswa_ayah,
                'siswa_ibu' => $row->siswa_ibu,
                'siswa_ayahkerja' => $row->siswa_ayahkerja,
                'siswa_ibukerja' => $row->siswa_ibukerja,
                'siswa_alamatortu' => $row->siswa_alamatortu,
                'user_wali' => $row->username,
            );
            $this->template->load('template', 'siswa_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('siswa'));
        }
    }

    public function create() {
         $user = $this->Siswa_model->getAlluser();
        $data = array(
            'user_data' => $user,
            'button' => 'Create',
            'action' => site_url('siswa/create_action'),
            'siswa_id' => set_value('siswa_id'),
            'siswa_nis' => set_value('siswa_nis'),
            'sub_id' => set_value('sub_id'),
            'tahun_id' => set_value('tahun_id'),
            'siswa_nama' => set_value('siswa_nama'),
            'siswa_jk' => set_value('siswa_jk'),
            'siswa_tempatlahir' => set_value('siswa_tempatlahir'),
            'siswa_tgllahir' => set_value('siswa_tgllahir'),
            'siswa_alamat' => set_value('siswa_alamat'),
            'siswa_ayah' => set_value('siswa_ayah'),
            'siswa_ibu' => set_value('siswa_ibu'),
            'siswa_ayahkerja' => set_value('siswa_ayahkerja'),
            'siswa_ibukerja' => set_value('siswa_ibukerja'),
            'siswa_alamatortu' => set_value('siswa_alamatortu'),
            'user_wali' => set_value('user_wali'),
        );
        $this->template->load('template', 'siswa_form', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'siswa_nis' => $this->input->post('siswa_nis', TRUE),
                'sub_id' => $this->input->post('sub_id', TRUE),
                'tahun_id' => $this->input->post('tahun_id', TRUE),
                'siswa_nama' => $this->input->post('siswa_nama', TRUE),
                'siswa_jk' => $this->input->post('siswa_jk', TRUE),
                'siswa_tempatlahir' => $this->input->post('siswa_tempatlahir', TRUE),
                'siswa_tgllahir' => $this->input->post('siswa_tgllahir', TRUE),
                'siswa_alamat' => $this->input->post('siswa_alamat', TRUE),
                'siswa_ayah' => $this->input->post('siswa_ayah', TRUE),
                'siswa_ibu' => $this->input->post('siswa_ibu', TRUE),
                'siswa_ayahkerja' => $this->input->post('siswa_ayahkerja', TRUE),
                'siswa_ibukerja' => $this->input->post('siswa_ibukerja', TRUE),
                'siswa_alamatortu' => $this->input->post('siswa_alamatortu', TRUE),
                'user_wali' => $this->input->post('user_wali', TRUE),
            );

            $this->Siswa_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('siswa'));
        }
    }

    public function update($id) {
        $row = $this->Siswa_model->get_by_id($id);
         $user = $this->Siswa_model->getAlluser();

        if ($row) {
            $data = array(
                 'user_data' => $user,
                'button' => 'Update',
                'action' => site_url('siswa/update_action'),
                'siswa_id' => set_value('siswa_id', $row->siswa_id),
                'siswa_nis' => set_value('siswa_nis', $row->siswa_nis),
                'sub_id' => set_value('sub_id', $row->sub_id),
                'tahun_id' => set_value('tahun_id', $row->tahun_id),
                'siswa_nama' => set_value('siswa_nama', $row->siswa_nama),
                'siswa_jk' => set_value('siswa_jk', $row->siswa_jk),
                'siswa_tempatlahir' => set_value('siswa_tempatlahir', $row->siswa_tempatlahir),
                'siswa_tgllahir' => set_value('siswa_tgllahir', $row->siswa_tgllahir),
                'siswa_alamat' => set_value('siswa_alamat', $row->siswa_alamat),
                'siswa_ayah' => set_value('siswa_ayah', $row->siswa_ayah),
                'siswa_ibu' => set_value('siswa_ibu', $row->siswa_ibu),
                'siswa_ayahkerja' => set_value('siswa_ayahkerja', $row->siswa_ayahkerja),
                'siswa_ibukerja' => set_value('siswa_ibukerja', $row->siswa_ibukerja),
                'siswa_alamatortu' => set_value('siswa_alamatortu', $row->siswa_alamatortu),
                'user_wali' => set_value('user_wali', $row->user_wali),
            );
            $this->template->load('template', 'siswa_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('siswa'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('siswa_id', TRUE));
        } else {
            $data = array(
                'siswa_nis' => $this->input->post('siswa_nis', TRUE),
                'sub_id' => $this->input->post('sub_id', TRUE),
                'tahun_id' => $this->input->post('tahun_id', TRUE),
                'siswa_nama' => $this->input->post('siswa_nama', TRUE),
                'siswa_jk' => $this->input->post('siswa_jk', TRUE),
                'siswa_tempatlahir' => $this->input->post('siswa_tempatlahir', TRUE),
                'siswa_tgllahir' => $this->input->post('siswa_tgllahir', TRUE),
                'siswa_alamat' => $this->input->post('siswa_alamat', TRUE),
                'siswa_ayah' => $this->input->post('siswa_ayah', TRUE),
                'siswa_ibu' => $this->input->post('siswa_ibu', TRUE),
                'siswa_ayahkerja' => $this->input->post('siswa_ayahkerja', TRUE),
                'siswa_ibukerja' => $this->input->post('siswa_ibukerja', TRUE),
                'siswa_alamatortu' => $this->input->post('siswa_alamatortu', TRUE),
                'user_wali' => $this->input->post('user_wali', TRUE),
            );

            $this->Siswa_model->update($this->input->post('siswa_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('siswa'));
        }
    }

    public function delete($id) {
        $row = $this->Siswa_model->get_by_id($id);

        if ($row) {
            $this->Siswa_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('siswa'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('siswa'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('siswa_nis', 'siswa nis', 'trim|required');
        $this->form_validation->set_rules('sub_id', 'sub id', 'trim|required');
        $this->form_validation->set_rules('tahun_id', 'tahun id', 'trim|required');
        $this->form_validation->set_rules('siswa_nama', 'siswa nama', 'trim|required');
        $this->form_validation->set_rules('siswa_jk', 'siswa jk', 'trim|required');
        $this->form_validation->set_rules('siswa_tempatlahir', 'siswa tempatlahir', 'trim|required');
        $this->form_validation->set_rules('siswa_tgllahir', 'siswa tgllahir', 'trim|required');
        $this->form_validation->set_rules('siswa_alamat', 'siswa alamat', 'trim|required');
        $this->form_validation->set_rules('siswa_ayah', 'siswa ayah', 'trim|required');
        $this->form_validation->set_rules('siswa_ibu', 'siswa ibu', 'trim|required');
        $this->form_validation->set_rules('siswa_ayahkerja', 'siswa ayahkerja', 'trim|required');
        $this->form_validation->set_rules('siswa_ibukerja', 'siswa ibukerja', 'trim|required');
        $this->form_validation->set_rules('siswa_alamatortu', 'siswa alamatortu', 'trim|required');
        $this->form_validation->set_rules('user_wali', 'user_wali', 'trim|required');

        $this->form_validation->set_rules('siswa_id', 'siswa_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "siswa.xls";
        $judul = "siswa";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "NIS");
        xlsWriteLabel($tablehead, $kolomhead++, "Kelas");
        xlsWriteLabel($tablehead, $kolomhead++, "Sub Kelas");
        xlsWriteLabel($tablehead, $kolomhead++, "Tahun Akademik");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama");
        xlsWriteLabel($tablehead, $kolomhead++, "Jenis Kelamin");
        xlsWriteLabel($tablehead, $kolomhead++, "Tempat Lahir");
        xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Lahir");
        xlsWriteLabel($tablehead, $kolomhead++, "Alamat");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Ayah");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Ibu");
        xlsWriteLabel($tablehead, $kolomhead++, "Pekerjaan Ayah");
        xlsWriteLabel($tablehead, $kolomhead++, "Pekerjaan Ibu");
        xlsWriteLabel($tablehead, $kolomhead++, "Alamat Orang Tua");

        foreach ($this->Siswa_model->get_all_query() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_nis);
            xlsWriteNumber($tablebody, $kolombody++, $data->kelas_nama);
            xlsWriteNumber($tablebody, $kolombody++, $data->sub_nama);
            xlsWriteNumber($tablebody, $kolombody++, $data->tahun_ket);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_nama);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_jk);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_tempatlahir);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_tgllahir);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_alamat);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_ayah);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_ibu);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_ayahkerja);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_ibukerja);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_alamatortu);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=siswa.doc");

        $data = array(
            'siswa_data' => $this->Siswa_model->get_all_query(),
            'start' => 0
        );

        $this->load->view('siswa_doc', $data);
    }

    function pdf() {
        $this->load->library('pdf');

        $data = array(
            'siswa_data' => $this->Siswa_model->get_all_query(),
            'start' => 0
        );

        $html = $this->load->view('siswa_doc', $data, true);

        $this->pdf->generate($html, 'siswa');
    }

}

/* End of file Siswa.php */
/* Location: ./application/controllers/Siswa.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-06-28 09:01:50 */
/* http://harviacode.com */