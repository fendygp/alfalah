<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upload_bukti extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('info_siswa_model');
        $this->load->model('Pembayaran_model');
        $this->load->model('Upload_bukti_model');
        $this->load->library('form_validation');
    }

    function index() {

        $user_id = $this->session->userdata('user_id');
        $siswa_wali = $this->info_siswa_model->getSiswawali($user_id);


        foreach ($siswa_wali as $a) {
            $siswa_nama = $a->siswa_nama;
            $wali_id = $a->user_wali;
            $siswa_id = $a->siswa_id;
            $tahun_id = $a->tahun_id;
            $kelas_id = $a->kelas_id;
        }
        $jenisbayar = $this->Pembayaran_model->getYangharusdibayar($tahun_id, $kelas_id);
        $totalnotif = $this->Pembayaran_model->countnotif($siswa_id);
         $getnotif = $this->Pembayaran_model->getnotif($siswa_id);

        $data = array(
            'action' => site_url('Upload_bukti/create_buktibayar'),
            'siswa_nama' => $siswa_nama,
            'wali_id' => $wali_id,
            'siswa_id' => $siswa_id,
            'user_id' => $user_id,
            'jenis_bayar' => $jenisbayar,
            'totalnotif' => $totalnotif,
            'getnotif' => $getnotif
        );
        $this->template->load('template', 'buktibayar_form', $data);
    }

    function create_buktibayar() {

        $config['upload_path'] = './uploads/';
        $config['overwrite'] = TRUE;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2048';
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['remove_spaces'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
//            $error = array('error' => $this->upload->display_errors());
//
            echo "<script>alert('Gagal upload!')</script>";
            redirect('Upload_bukti', 'refresh');
        } else {
            $upload_data = $this->upload->data();
            $gambar = $upload_data['file_name'];

            $link = 'http://' . $_SERVER['SERVER_NAME'] . '/alfalahkeu/uploads/' . $gambar;
            $data = array(
                'wali_id' => $this->input->post('wali_id', TRUE),
                'siswa_id' => $this->input->post('siswa_id', TRUE),
                'link_photo' => $link,
                'keterangan' => $this->input->post('keterangan', TRUE)
            );

            // model
            $this->Upload_bukti_model->insert($data);
            echo "<script>alert('Upload! Sukses')</script>";
            redirect('Upload_bukti', 'refresh');

//            $user_id = $this->session->userdata('user_id');
//            $siswa_wali = $this->info_siswa_model->getSiswawali($user_id);
//
//            foreach ($siswa_wali as $a) {
//                $siswa_nama = $a->siswa_nama;
//                $wali_id = $a->wali_id;
//                $siswa_id = $a->siswa_id;
//            }
//
//            $data2 = array(
//                'action' => site_url('Upload_bukti/create_buktibayar'),
//                'siswa_nama' => $siswa_nama,
//                'wali_id' => $wali_id,
//                'siswa_id' => $siswa_id,
//                'user_id' => $user_id
//            );
//            $this->template->load('template', 'buktibayar_form', $data2);
        }
    }

}
