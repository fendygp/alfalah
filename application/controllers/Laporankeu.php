<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporankeu extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Laporankeu_model');
        $this->load->library('form_validation');
    }

    function index() {

        $data = array(
            'action' => site_url('Laporankeu/pilihjenislapor'),
        );
        $this->template->load('template', 'laporankeu_form', $data);
    }

    function pilihjenislapor2() {
        $jenislapor = $this->input->post('jenis_lapor');
        if ($jenislapor == "harian") {
            $lapordata = $this->Laporankeu_model->laporPerhari();
            $data = array(
                'action' => site_url('Laporankeu/pilihjenislapor2'),
                'lapordata' => $lapordata
            );
            $this->template->load('template', 'laporankeu_harilist2', $data);
        } elseif ($jenislapor == "bulanan") {
            $lapordata = $this->Laporankeu_model->laporPerbulan();
            $data = array(
                'action' => site_url('Laporankeu/pilihjenislapor2'),
                'lapordata' => $lapordata
            );
            $this->template->load('template', 'laporankeu_bulanlist2', $data);
        } elseif ($jenislapor == "tahunan") {
            $lapordata = $this->Laporankeu_model->laporPertahun();
            $data = array(
                'action' => site_url('Laporankeu/pilihjenislapor2'),
                'lapordata' => $lapordata
            );
            $this->template->load('template', 'laporankeu_tahunlist2', $data);
        }
    }

    function pilihjenislapor() {
        $jenislapor = $this->input->post('jenis_lapor');

        if ($jenislapor == "harian") {
            $data = array(
                'action' => site_url('Laporankeu/harianlist')
            );
            $this->template->load('template', 'laporankeu_hari', $data);
        } elseif ($jenislapor == "bulanan") {

            $data = array(
                'action' => site_url('Laporankeu/bulananlist')
            );
            $this->template->load('template', 'laporankeu_bulan', $data);
        } elseif ($jenislapor == "tahunan") {
            $tahunakademik = $this->Laporankeu_model->getTahunAkademik();
            $data = array(
                'tahundata' => $tahunakademik,
                'action' => site_url('Laporankeu/tahunanlist')
            );
            $this->template->load('template', 'laporankeu_tahun', $data);
        }
    }

    function harianlist() {
        $tanggalawal = $this->input->post('tanggalawal');
        $tanggalakhir = $this->input->post('tanggalakhir');
        $hariandata = $this->Laporankeu_model->getHarian($tanggalawal, $tanggalakhir);
        $total1 = $this->Laporankeu_model->jumlahMasuktgl($tanggalawal, $tanggalakhir);
        foreach ($total1 as $t) {
            $total = $t->totalmasuk;
        }

        $data = array(
            'action' => site_url('Laporankeu/harianlist'),
            'hariandata' => $hariandata,
            'total' => $total,
            'tanggalawal' => $tanggalawal,
            'tanggalakhir' => $tanggalakhir,
        );

        $this->template->load('template', 'laporankeu_harilist', $data);
    }

    function bulananlist() {
        $bulanawal = $this->input->post('bulanawal');
        $bulanakhir = $this->input->post('bulanakhir');
        $hariandata = $this->Laporankeu_model->getBulanan($bulanawal, $bulanakhir);
        $total1 = $this->Laporankeu_model->jumlahMasukbln($bulanawal, $bulanakhir);
        foreach ($total1 as $t) {
            $total = $t->totalmasuk;
        }


        $data = array(
            'action' => site_url('Laporankeu/bulananlist'),
            'hariandata' => $hariandata,
            'total' => $total,
            'bulanawal' => $bulanawal,
            'bulanakhir' => $bulanakhir
        );

        $this->template->load('template', 'laporankeu_bulanlist', $data);
    }

    function tahunanlist() {
        $tahunawal = $this->input->post('tahunawal');
        $tahunakhir = $this->input->post('tahunakhir');
        $hariandata = $this->Laporankeu_model->getTahunan($tahunawal, $tahunakhir);
        $total1 = $this->Laporankeu_model->jumlahMasukthn($tahunawal, $tahunakhir);
        foreach ($total1 as $t) {
            $total = $t->totalmasuk;
        }

        $tahunakademik = $this->Laporankeu_model->getTahunAkademik();
        $data = array(
            'action' => site_url('Laporankeu/tahunanlist'),
            'hariandata' => $hariandata,
            'total' => $total,
            'tahundata' => $tahunakademik,
            'tahunawal' => $tahunawal,
            'tahunakhir' => $tahunakhir
        );

        $this->template->load('template', 'laporankeu_tahunlist', $data);
    }

    function wordhari() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=laporan_harian.doc");

        $tanggalawal = $this->uri->segment(3);
        $tanggalakhir = $this->uri->segment(4);
        $hariandata = $this->Laporankeu_model->getHarian($tanggalawal, $tanggalakhir);
        $data = array(
            'hariandata' => $hariandata
        );

        $this->load->view('laporankeu_harilist_doc', $data);
    }

    public function excelhari() {
        $tanggalawal = $this->uri->segment(3);
        $tanggalakhir = $this->uri->segment(4);
        $hariandata = $this->Laporankeu_model->getHarian($tanggalawal, $tanggalakhir);
        $this->load->helper('exportexcel');
        $namaFile = "laporan_harian.xls";
        $judul = "laporan harian";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "NIS");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama");
        xlsWriteLabel($tablehead, $kolomhead++, "Jenis Bayar");
        xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Bayar");
        xlsWriteLabel($tablehead, $kolomhead++, "Jumlah Pemasukan");



        foreach ($hariandata as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_nis);
            xlsWriteNumber($tablebody, $kolombody++, $data->siswa_nama);
            xlsWriteNumber($tablebody, $kolombody++, $data->jenisbayar_ket);
            xlsWriteNumber($tablebody, $kolombody++, $data->pembayaran_tanggal);
            xlsWriteLabel($tablebody, $kolombody++, $data->pembayaran_jumlah);


            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdfhari() {
        $this->load->library('pdf');

        $tanggalawal = $this->uri->segment(3);
        $tanggalakhir = $this->uri->segment(4);
        $hariandata = $this->Laporankeu_model->getHarian($tanggalawal, $tanggalakhir);
        $data = array(
            'hariandata' => $hariandata
        );

        $html = $this->load->view('laporankeu_harilist_doc', $data, true);
        $this->pdf->generate($html, 'laporanharian');
    }
    
    function wordbulan() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=laporan_bulanan.doc");

        $tanggalawal = $this->uri->segment(3);
        $tanggalakhir = $this->uri->segment(4);
        $hariandata = $this->Laporankeu_model->getBulanan($tanggalawal, $tanggalakhir);
        $data = array(
            'hariandata' => $hariandata
        );

        $this->load->view('laporankeu_harilist_doc', $data);
    }

    public function excelbulan() {
        $tanggalawal = $this->uri->segment(3);
        $tanggalakhir = $this->uri->segment(4);
        $hariandata = $this->Laporankeu_model->getBulanan($tanggalawal, $tanggalakhir);
        $this->load->helper('exportexcel');
        $namaFile = "laporan_bulanan.xls";
        $judul = "laporan bulanan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "NIS");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama");
        xlsWriteLabel($tablehead, $kolomhead++, "Jenis Bayar");
        xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Bayar");
        xlsWriteLabel($tablehead, $kolomhead++, "Jumlah Pemasukan");



        foreach ($hariandata as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_nis);
            xlsWriteNumber($tablebody, $kolombody++, $data->siswa_nama);
            xlsWriteNumber($tablebody, $kolombody++, $data->jenisbayar_ket);
            xlsWriteNumber($tablebody, $kolombody++, $data->pembayaran_tanggal);
            xlsWriteLabel($tablebody, $kolombody++, $data->pembayaran_jumlah);


            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdfbulan() {
        $this->load->library('pdf');

        $tanggalawal = $this->uri->segment(3);
        $tanggalakhir = $this->uri->segment(4);
        $hariandata = $this->Laporankeu_model->getBulanan($tanggalawal, $tanggalakhir);
        $data = array(
            'hariandata' => $hariandata
        );

        $html = $this->load->view('laporankeu_harilist_doc', $data, true);
        $this->pdf->generate($html, 'laporanbulanan');
    }
    
    function wordtahun() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=laporan_tahunan.doc");

        $tanggalawal = $this->uri->segment(3);
        $tanggalakhir = $this->uri->segment(4);
        $hariandata = $this->Laporankeu_model->getTahunan($tanggalawal, $tanggalakhir);
        $data = array(
            'hariandata' => $hariandata
        );

        $this->load->view('laporankeu_harilist_doc', $data);
    }

    public function exceltahun() {
        $tanggalawal = $this->uri->segment(3);
        $tanggalakhir = $this->uri->segment(4);
        $hariandata = $this->Laporankeu_model->getTahunan($tanggalawal, $tanggalakhir);
        $this->load->helper('exportexcel');
        $namaFile = "laporan_tahunan.xls";
        $judul = "laporan tahunan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "NIS");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama");
        xlsWriteLabel($tablehead, $kolomhead++, "Jenis Bayar");
        xlsWriteLabel($tablehead, $kolomhead++, "Tanggal Bayar");
        xlsWriteLabel($tablehead, $kolomhead++, "Jumlah Pemasukan");



        foreach ($hariandata as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->siswa_nis);
            xlsWriteNumber($tablebody, $kolombody++, $data->siswa_nama);
            xlsWriteNumber($tablebody, $kolombody++, $data->jenisbayar_ket);
            xlsWriteNumber($tablebody, $kolombody++, $data->pembayaran_tanggal);
            xlsWriteLabel($tablebody, $kolombody++, $data->pembayaran_jumlah);


            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdftahun() {
        $this->load->library('pdf');

        $tanggalawal = $this->uri->segment(3);
        $tanggalakhir = $this->uri->segment(4);
        $hariandata = $this->Laporankeu_model->getTahunan($tanggalawal, $tanggalakhir);
        $data = array(
            'hariandata' => $hariandata
        );

        $html = $this->load->view('laporankeu_harilist_doc', $data, true);
        $this->pdf->generate($html, 'laporantahunan');
    }

}

/* End of file Pembayaran.php */
/* Location: ./application/controllers/Pembayaran.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-07-22 05:30:14 */
/* http://harviacode.com */