<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aduan_wali extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('info_siswa_model');
        $this->load->model('Pembayaran_model');
        $this->load->model('Upload_bukti_model');
    }

    function index() {

        $aduan = $this->Upload_bukti_model->get_all();
        $data = array(
          'aduan_data' =>$aduan
        );
        $this->template->load('template', 'aduan_wali_list', $data);
    }
    
    function bayarsiswa($nis) {
       
        $cek = $this->Pembayaran_model->getSiswa($nis);

        foreach ($cek as $a) {
            $siswa_id = $a->siswa_id;
            $tahun_id = $a->tahun_id;
            $kelas_id = $a->kelas_id;
        }
        $pembayaransiswa = $this->Pembayaran_model->getPembayaransiswa($siswa_id);
        $pembayaransiswa2 = $this->Pembayaran_model->getPembayaransiswa2($siswa_id);
        $biaya_sekolah = $this->Pembayaran_model->getYangharusdibayar($tahun_id, $kelas_id);


        if ($cek == TRUE) {
            $data = array(
                'action' => site_url('pembayaran/databayar_siswa'),
                'action2' => site_url('pembayaran/jumlah_bayar'),
                'jenisbayar_data' => $this->Pembayaran_model->getJenisbayar(),
                'siswa_data' => $cek,
                'yangdibayar_data' => $biaya_sekolah,
                'pembayaransiswa_data' => $pembayaransiswa,
                'pembayaransiswa_data2' => $pembayaransiswa2
            );

            $this->template->load('template', 'pembayaran_list', $data);
        } else {
            echo "<script>alert('Data tidak ditemukan!')</script>";
            redirect('pembayaran', 'refresh');
        }
    }
    
    

    

}
