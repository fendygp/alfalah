<?php

error_reporting(0);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Apialfalah extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('auth');
        $this->load->model('Auth_model');
        $this->load->model('Encryption');
        $this->load->model('Pembayaran_model');
        $this->load->helper('form', 'url');
        $this->load->model('Laporankeu_model');
        $this->load->model('Info_siswa_model');
    }

    function tes() {
        $tes1 = $this->input->post('tes');

        $data = $tes1;

        if ($tes1 != NULL) {
            echo json_encode(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'));
        } else {
            echo json_encode(array('status' => false, 'data' => $data, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function login() {
        $username = $this->input->post("username");
        $passwordtemp = $this->input->post("password");
        $password = $this->Encryption->encode($passwordtemp);

        $cek = $this->Auth_model->cek_user($username, $password); //melakukan persamaan data dengan database

        if (count($cek) == 1) { //cek data berdasarkan username & pass
            echo json_encode(array('status' => true, 'data' => $cek, 'msg' => 'get bangunan berhasil!!'));
        } else {
            echo json_encode(array('status' => false, 'data' => $cek, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function infosiswa() {
        $user_id = $this->input->post("user_id");
        $siswa_wali = $this->Info_siswa_model->getSiswawali($user_id);

        if ($siswa_wali != NULL) {
            echo json_encode(array('status' => true, 'data' => $siswa_wali, 'msg' => 'get bangunan berhasil!!'));
        } else {
            echo json_encode(array('status' => false, 'data' => $siswa_wali, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function infobayar() {
        $user_id = $this->input->post("user_id");
        $siswa_wali = $this->Info_siswa_model->getSiswawali($user_id);
        foreach ($siswa_wali as $a) {
            $siswa_id = $a->siswa_id;
        }
        $pembayaransiswa = $this->Pembayaran_model->getPembayaransiswa($siswa_id);


        if ($siswa_wali != NULL) {
            echo json_encode(array('status' => true, 'data' => $pembayaransiswa, 'msg' => 'get bangunan berhasil!!'));
        } else {
            echo json_encode(array('status' => false, 'data' => $pembayaransiswa, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function laporhari() {
        $tanggalawal = $this->input->post('tanggalawal');
        $tanggalakhir = $this->input->post('tanggalakhir');

        $data = $this->Laporankeu_model->persenPerhari($tanggalawal, $tanggalakhir);


        if ($data != NULL) {
            echo json_encode(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'));
        } else {
            echo json_encode(array('status' => false, 'data' => $data, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function laporbulan() {
        $bulanawal = $this->input->post('bulanawal');
        $bulanakhir = $this->input->post('bulanakhir');

        $data = $this->Laporankeu_model->persenPerbulan($bulanawal, $bulanakhir);

        if ($data != NULL) {
            echo json_encode(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'));
        } else {
            echo json_encode(array('status' => false, 'data' => $data, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function laportahun() {
        $tahunawal = $this->input->post('tahunawal');
        $tahunakhir = $this->input->post('tahunakhir');

        $data = $this->Laporankeu_model->persenPertahun($tahunawal, $tahunakhir);
        if ($data != NULL) {
            echo json_encode(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'));
        } else {
            echo json_encode(array('status' => false, 'data' => $data, 'msg' => 'get bangunan gagal!!'));
        }
    }

    function upload_buktibayar() {
        $config['upload_path'] = './uploads/';
        $config['overwrite'] = TRUE;
        $config['allowed_types'] = '*';
        $config['max_size'] = '2048';
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['remove_spaces'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
//            $error = array('error' => $this->upload->display_errors());
//
            echo "<script>alert('Gagal upload!')</script>";
            redirect('Upload_bukti', 'refresh');
        } else {
            $upload_data = $this->upload->data();
            $gambar = $upload_data['file_name'];

            $link = 'http://' . $_SERVER['SERVER_NAME'] . '/alfalahkeu/uploads/' . $gambar;
            $data = array(
                'wali_id' => $this->input->post('wali_id', TRUE),
                'siswa_id' => $this->input->post('siswa_id', TRUE),
                'link_photo' => $link,
                'keterangan' => $this->input->post('keterangan', TRUE)
            );

            // model
            $data = $this->Upload_bukti_model->insert($data);
            if ($data != NULL) {
                echo json_encode(array('status' => true, 'data' => $data, 'msg' => 'get bangunan berhasil!!'));
            } else {
                echo json_encode(array('status' => false, 'data' => $data, 'msg' => 'get bangunan gagal!!'));
            }
        }
    }

}
