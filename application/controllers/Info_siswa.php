<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Info_siswa extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Info_siswa_model');
        $this->load->model('Pembayaran_model');
        $this->load->library('form_validation');
    }

    function index() {

        $user_id = $this->session->userdata('user_id');
        $siswa_wali = $this->Info_siswa_model->getSiswawali($user_id);



        foreach ($siswa_wali as $a) {
            $siswa_id = $a->siswa_id;
            $tahun_id = $a->tahun_id;
            $kelas_id = $a->kelas_id;
        }
        $pembayaransiswa = $this->Pembayaran_model->getPembayaransiswa($siswa_id);
        $pembayaransiswa2 = $this->Pembayaran_model->getPembayaransiswa2($siswa_id);
        $biaya_sekolah = $this->Pembayaran_model->getYangharusdibayar($tahun_id, $kelas_id);
        $totalnotif = $this->Pembayaran_model->countnotif($siswa_id);
        $getnotif = $this->Pembayaran_model->getnotif($siswa_id);




        $data = array(
            'user_id' => $user_id,
            'siswa_data' => $siswa_wali,
            'yangdibayar_data' => $biaya_sekolah,
            'pembayaransiswa_data' => $pembayaransiswa,
            'pembayaransiswa_data2' => $pembayaransiswa2,
            'totalnotif' => $totalnotif,
            'getnotif' => $getnotif
        );
        $this->template->load('template', 'info_siswa_list', $data);
    }

}
