<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Pembayaran_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $jenisbayar = $this->Pembayaran_model->getJenisbayar();

        $data = array(
            'action' => site_url('pembayaran/databayar_siswa'),
            'jenisbayar_data' => $jenisbayar
        );
        $this->template->load('template', 'pembayaran_form', $data);
    }

    public function read($id) {
        $row = $this->Pembayaran_model->get_by_id($id);
        if ($row) {
            $data = array(
                'pembayaran_id' => $row->pembayaran_id,
                'siswa_id' => $row->siswa_id,
                'biayasekolah_id' => $row->biayasekolah_id,
                'pembayaran_tanggal' => $row->pembayaran_tanggal,
                'pembayaran_jumlah' => $row->pembayaran_jumlah,
            );
            $this->load->view('pembayaran/pembayaran_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pembayaran'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('pembayaran/create_action'),
            'pembayaran_id' => set_value('pembayaran_id'),
            'siswa_id' => set_value('siswa_id'),
            'biayasekolah_id' => set_value('biayasekolah_id'),
            'pembayaran_tanggal' => set_value('pembayaran_tanggal'),
            'pembayaran_jumlah' => set_value('pembayaran_jumlah'),
        );
        $this->load->view('pembayaran/pembayaran_form', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'siswa_id' => $this->input->post('siswa_id', TRUE),
                'biayasekolah_id' => $this->input->post('biayasekolah_id', TRUE),
                'pembayaran_tanggal' => $this->input->post('pembayaran_tanggal', TRUE),
                'pembayaran_jumlah' => $this->input->post('pembayaran_jumlah', TRUE),
            );

            $this->Pembayaran_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pembayaran'));
        }
    }

    public function update($id) {
        $row = $this->Pembayaran_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('pembayaran/update_action'),
                'pembayaran_id' => set_value('pembayaran_id', $row->pembayaran_id),
                'siswa_id' => set_value('siswa_id', $row->siswa_id),
                'biayasekolah_id' => set_value('biayasekolah_id', $row->biayasekolah_id),
                'pembayaran_tanggal' => set_value('pembayaran_tanggal', $row->pembayaran_tanggal),
                'pembayaran_jumlah' => set_value('pembayaran_jumlah', $row->pembayaran_jumlah),
            );
            $this->load->view('pembayaran/pembayaran_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pembayaran'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('pembayaran_id', TRUE));
        } else {
            $data = array(
                'siswa_id' => $this->input->post('siswa_id', TRUE),
                'biayasekolah_id' => $this->input->post('biayasekolah_id', TRUE),
                'pembayaran_tanggal' => $this->input->post('pembayaran_tanggal', TRUE),
                'pembayaran_jumlah' => $this->input->post('pembayaran_jumlah', TRUE),
            );

            $this->Pembayaran_model->update($this->input->post('pembayaran_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pembayaran'));
        }
    }

    public function delete($id) {
        $row = $this->Pembayaran_model->get_by_id($id);

        if ($row) {
            $this->Pembayaran_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pembayaran'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pembayaran'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('siswa_id', 'siswa id', 'trim|required');
        $this->form_validation->set_rules('biayasekolah_id', 'biayasekolah id', 'trim|required');
        $this->form_validation->set_rules('pembayaran_tanggal', 'pembayaran tanggal', 'trim|required');
        $this->form_validation->set_rules('pembayaran_jumlah', 'pembayaran jumlah', 'trim|required');

        $this->form_validation->set_rules('pembayaran_id', 'pembayaran_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "pembayaran.xls";
        $judul = "pembayaran";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Siswa Id");
        xlsWriteLabel($tablehead, $kolomhead++, "Biayasekolah Id");
        xlsWriteLabel($tablehead, $kolomhead++, "Pembayaran Tanggal");
        xlsWriteLabel($tablehead, $kolomhead++, "Pembayaran Jumlah");

        foreach ($this->Pembayaran_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->siswa_id);
            xlsWriteNumber($tablebody, $kolombody++, $data->biayasekolah_id);
            xlsWriteLabel($tablebody, $kolombody++, $data->pembayaran_tanggal);
            xlsWriteNumber($tablebody, $kolombody++, $data->pembayaran_jumlah);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=pembayaran.doc");

        $data = array(
            'pembayaran_data' => $this->Pembayaran_model->get_all(),
            'start' => 0
        );

        $this->load->view('pembayaran_doc', $data);
    }

    function databayar_siswa() {
        $nis = $this->input->post('nis');
        $cek = $this->Pembayaran_model->getSiswa($nis);

        foreach ($cek as $a) {
            $siswa_id = $a->siswa_id;
            $tahun_id = $a->tahun_id;
            $kelas_id = $a->kelas_id;
        }
        $pembayaransiswa = $this->Pembayaran_model->getPembayaransiswa($siswa_id);
        $pembayaransiswa2 = $this->Pembayaran_model->getPembayaransiswa2($siswa_id);
        $biaya_sekolah = $this->Pembayaran_model->getYangharusdibayar($tahun_id, $kelas_id);


        if ($cek == TRUE) {
            $data = array(
                'action' => site_url('pembayaran/databayar_siswa'),
                'action2' => site_url('pembayaran/jumlah_bayar'),
                'jenisbayar_data' => $this->Pembayaran_model->getJenisbayar(),
                'siswa_data' => $cek,
                'yangdibayar_data' => $biaya_sekolah,
                'pembayaransiswa_data' => $pembayaransiswa,
                'pembayaransiswa_data2' => $pembayaransiswa2
            );

            $this->template->load('template', 'pembayaran_list', $data);
        } else {
            echo "<script>alert('Data tidak ditemukan!')</script>";
            redirect('pembayaran', 'refresh');
        }
    }

    public function jumlah_bayar() {
        $nis = $this->input->post('nis');
        $siswa_id = $this->input->post('siswa_id');
        $biayasekolah_id = $this->input->post('biayasekolah_id');


        $cek = $this->Pembayaran_model->getSiswa($nis);
        foreach ($cek as $a) {
            $siswa_id = $a->siswa_id;
            $tahun_id = $a->tahun_id;
            $kelas_id = $a->kelas_id;
        }

        $pembayaransiswa = $this->Pembayaran_model->getPembayaransiswa($siswa_id);
        $pembayaransiswa2 = $this->Pembayaran_model->getPembayaransiswa2($siswa_id);
        $biaya_sekolah = $this->Pembayaran_model->getYangharusdibayar($tahun_id, $kelas_id);
        $jumlahbiaya_sekolah = $this->Pembayaran_model->getBiaya($biayasekolah_id);
        $jenisbayarsudah = $this->Pembayaran_model->jumlahperjenisbayar($siswa_id, $biayasekolah_id);


        foreach ($jenisbayarsudah as $z) {
            $sudahbayar = $z->jumlahpembayaran;
        }


        foreach ($jumlahbiaya_sekolah as $y) {
            $jumlahbiaya = $y->biayasekolah_jumlah;
            $jenisbayar_ket = $y->jenisbayar_ket;
        }

        $sisa = $jumlahbiaya - $sudahbayar;
        if ($cek == TRUE) {
            $data = array(
                'action' => site_url('pembayaran/data_siswa'),
                'action2' => site_url('pembayaran/tambah_bayar'),
                'jenisbayar_data' => $this->Pembayaran_model->getJenisbayar(),
                'siswa_data' => $cek,
                'yangdibayar_data' => $biaya_sekolah,
                'pembayaransiswa_data' => $pembayaransiswa,
                'pembayaransiswa_data2' => $pembayaransiswa2,
                'biayasekolah_id' => $biayasekolah_id,
                'jumlahbiaya' => $jumlahbiaya,
                'jenisbayar_ket' => $jenisbayar_ket,
                'sisa' => $sisa
            );

            $this->template->load('template', 'pembayaran_list2', $data);
        } else {
            echo "<script>alert('Data tidak ditemukan!')</script>";
            redirect('pembayaran', 'refresh');
        }
    }

    public function tambah_bayar() {
        $nis = $this->input->post('nis');
        $siswa_id = $this->input->post('siswa_id');
        $biayasekolah_id = $this->input->post('biayasekolah_id');
        $pembayaran_jumlah = $this->input->post('jumlah_bayar');
        $tanggal = $this->input->post('tanggal');
        $jumlahbiaya_sekolah = $this->Pembayaran_model->getBiaya($biayasekolah_id);

        $cek = $this->Pembayaran_model->getSiswa($nis);

        foreach ($cek as $a) {
            $siswa_id = $a->siswa_id;
            $tahun_id = $a->tahun_id;
            $kelas_id = $a->kelas_id;
        }


        $biaya_sekolah = $this->Pembayaran_model->getYangharusdibayar($tahun_id, $kelas_id);
        $jenisbayarsudah = $this->Pembayaran_model->jumlahperjenisbayar($siswa_id, $biayasekolah_id);

        foreach ($jumlahbiaya_sekolah as $y) {
            $jumlahbiaya = $y->biayasekolah_jumlah;
        }

        foreach ($jenisbayarsudah as $z) {
            $sudahbayar = $z->jumlahpembayaran;
        }
        $sisa = $jumlahbiaya - $sudahbayar;
        $n = $sisa - $pembayaran_jumlah;
        if ($n == 0) {
            $statusbayar = 'lunas';
        } else {
             $statusbayar = 'belum lunas';
        }

        $data = array(
            'siswa_id' => $siswa_id,
            'biayasekolah_id' => $biayasekolah_id,
            'pembayaran_tanggal' => $tanggal,
            'pembayaran_jumlah' => $pembayaran_jumlah,
            'sisa' => $sisa,
            'statusbayar' => $statusbayar
        );

        $tambah = $this->Pembayaran_model->insert($data);
        $data2 = array(
            'siswa_id' => $siswa_id,
            'biayasekolah_id' => $biayasekolah_id,
        );

        $this->Pembayaran_model->insert_notif($data2);
        $pembayaransiswa = $this->Pembayaran_model->getPembayaransiswa($siswa_id);
        $pembayaransiswa2 = $this->Pembayaran_model->getPembayaransiswa2($siswa_id);


        if ($cek == TRUE) {
            if ($tambah == TRUE) {
                $data = array(
                    'action' => site_url('pembayaran/data_siswa'),
                    'action2' => site_url('pembayaran/jumlah_bayar'),
                    'jenisbayar_data' => $this->Pembayaran_model->getJenisbayar(),
                    'siswa_data' => $cek,
                    'yangdibayar_data' => $biaya_sekolah,
                    'pembayaransiswa_data' => $pembayaransiswa,
					'pembayaransiswa_data2' => $pembayaransiswa2,
                    'biayasekolah_id' => $biayasekolah_id,
                    'nis' => $nis,
                    'siswa_id' => $siswa_id
                );

                echo "<script>alert('Data berhasil ditambahkan!')</script>";
                $this->template->load('template', 'pembayaran_list', $data);
            } else {
                echo "<script>alert('Data gagal ditambahkan!')</script>";
                redirect('pembayaran', 'refresh');
            }
        } else {
            echo "<script>alert('Data tidak ditemukan!')</script>";
            redirect('pembayaran', 'refresh');
        }
    }

//    public function data_siswa() {
//        $nis = $this->input->post('nis');
//        $cek = $this->Pembayaran_model->getSiswa($nis);
//
//        foreach ($cek as $a) {
//            // $siswa_id = $a->siswa_id;
//            $tahun_id = $a->tahun_id;
//            $kelas_id = $a->kelas_id;
//        }
//        $cekbiaya = $this->Pembayaran_model->getYangharusdibayar($tahun_id, $kelas_id);
//
//        if ($cek == TRUE) {
//            $data = array(
//                'action' => site_url('pembayaran/data_siswa'),
//                'action2' => site_url('pembayaran/tambah_bayar'),
//                'jenisbayar_data' => $this->Pembayaran_model->getJenisbayar(),
//                'siswa_data' => $cek,
//                'yangdibayar_data' => $cekbiaya
//            );
//
//            $this->template->load('template', 'pembayaran_list', $data);
//        } else {
//            echo "<script>alert('Data tidak ditemukan!')</script>";
//            redirect('pembayaran', 'refresh');
//        }
//    }
//
//    public function tambah_bayar() {
//        $nis = $this->input->post('nis');
//        $jumlah_bayar = $this->input->post('jumlah_bayar');
//        $siswa_id = $this->input->post('siswa_id');
//        $date = $this->input->post('date');
//        $biayasekolah_id = $this->input->post('biayasekolah_id');
//
//        $data = array(
//            'pembayaran_jumlah' => $jumlah_bayar,
//            'siswa_id' => $siswa_id,
//            'pembayaran_tanggal' => $date,
//            'biayasekolah_id' => $biayasekolah_id
//        );
//
//        
//        $this->Pembayaran_model->insert($data);
//
//
//        $cek = $this->Pembayaran_model->getSiswa($nis);
//
//        foreach ($cek as $a) {
//            // $siswa_id = $a->siswa_id;
//            $tahun_id = $a->tahun_id;
//            $kelas_id = $a->kelas_id;
//        }
//
//        $cekbiaya = $this->Pembayaran_model->getYangharusdibayar($tahun_id, $kelas_id);
//
//        if ($cek == TRUE) {
//            $data = array(
//                'action' => site_url('pembayaran/data_siswa'),
//                'action2' => site_url('pembayaran/tambah_bayar'),
//                'jenisbayar_data' => $this->Pembayaran_model->getJenisbayar(),
//                'siswa_data' => $cek,
//                'biayasekolah_id' => $biayasekolah_id,
//                'yangdibayar_data' => $cekbiaya
//            );
//
//            $this->template->load('template', 'pembayaran_list', $data);
//        } else {
//            echo "<script>alert('Data tidak ditemukan!')</script>";
//            redirect('pembayaran', 'refresh');
//        }
//    }
}

/* End of file Pembayaran.php */
/* Location: ./application/controllers/Pembayaran.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-07-22 05:30:14 */
/* http://harviacode.com */