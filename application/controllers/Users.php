<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Users_model');
        $this->load->library('form_validation');
        $this->load->model('Encryption');
    }

    public function index() {
        $users = $this->Users_model->get_all();

        $data = array(
            'users_data' => $users
        );

        $this->template->load('template', 'users_list', $data);
    }

    public function read($id) {
        $row = $this->Users_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'username' => $row->username,
                'password' => $this->Encryption->decode($row->password),
                'name' => $row->name,
                'phone' => $row->phone,
            );
            $this->template->load('template', 'users_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('users/create_action'),
            'id' => set_value('id'),
            'username' => set_value('username'),
            'password' => set_value('password'),
            'name' => set_value('name'),
            'phone' => set_value('phone'),
        );
        $this->template->load('template', 'users_form', $data);
    }

    public function create_action() {
        //$this->_rules();
//
//        if ($this->form_validation->run() == FALSE) {
//            $this->create();
//        } else {
            $data = array(
                'username' => $this->input->post('username', TRUE),
                'password' => $this->Encryption->encode($this->input->post('password', TRUE)),
                'name' => $this->input->post('name', TRUE),
                'phone' => $this->input->post('phone', TRUE),
            );

            $this->Users_model->insert($data);
//            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('users'));
//        }
    }

    public function update($id) {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('users/update_action'),
                'id' => set_value('id', $row->id),
                'username' => set_value('username', $row->username),
                'password' => set_value('password', $this->Encryption->decode($row->password)),
                'name' => set_value('name', $row->name),
                'phone' => set_value('phone', $row->phone),
            );
            $this->template->load('template', 'users_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }

    public function update_action() {
//        $this->_rules();
//
//        if ($this->form_validation->run() == FALSE) {
//            $this->update($this->input->post('id', TRUE));
//        } else {
            $data = array(
                'username' => $this->input->post('username', TRUE),
                'password' => $this->Encryption->encode($this->input->post('password', TRUE)),
                'name' => $this->input->post('name', TRUE),
                'phone' => $this->input->post('phone', TRUE),
            );

            $this->Users_model->update($this->input->post('id', TRUE), $data);
//            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('users'));
//        }
    }

    public function delete($id) {
        $row = $this->Users_model->get_by_id($id);

        if ($row) {
            $this->Users_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('users'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('users'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('ip_address', 'ip address', 'trim|required');
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');

        $this->form_validation->set_rules('name', 'first name', 'trim|required');

        $this->form_validation->set_rules('phone', 'phone', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "users.xls";
        $judul = "users";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");

        xlsWriteLabel($tablehead, $kolomhead++, "Username");
        xlsWriteLabel($tablehead, $kolomhead++, "Password");

        xlsWriteLabel($tablehead, $kolomhead++, "Name");

        xlsWriteLabel($tablehead, $kolomhead++, "Phone");

        foreach ($this->Users_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);

            xlsWriteLabel($tablebody, $kolombody++, $data->username);
            xlsWriteLabel($tablebody, $kolombody++, $data->password);
            xlsWriteLabel($tablebody, $kolombody++, $data->salt);
            xlsWriteLabel($tablebody, $kolombody++, $data->email);
            xlsWriteLabel($tablebody, $kolombody++, $this->Encryption->decode($data->activation_code));

            xlsWriteLabel($tablebody, $kolombody++, $data->name);

            xlsWriteLabel($tablebody, $kolombody++, $data->phone);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=users.doc");

        $data = array(
            'users_data' => $this->Users_model->get_all(),
            'start' => 0
        );

        $this->template->load('template', 'users_doc', $data);
    }

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-07-17 19:29:20 */
/* http://harviacode.com */
