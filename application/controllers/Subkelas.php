<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subkelas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Subkelas_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $subkelas = $this->Subkelas_model->get_all_query();

        $data = array(
            'subkelas_data' => $subkelas
        );

        $this->template->load('template', 'subkelas_list', $data);
    }

    public function read($id) {
        $row = $this->Subkelas_model->get_by_id($id);
        if ($row) {
            $data = array(
                'sub_id' => $row->sub_id,
                'sub_nama' => $row->sub_nama,
                'kelas_nama' => $row->kelas_nama,
            );
            $this->template->load('template', 'subkelas_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('subkelas'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('subkelas/create_action'),
            'sub_id' => set_value('sub_id'),
            'sub_nama' => set_value('sub_nama'),
            'kelas_id' => set_value('kelas_id'),
        );
        $this->template->load('template', 'subkelas_form', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'sub_nama' => $this->input->post('sub_nama', TRUE),
                'kelas_id' => $this->input->post('kelas_id', TRUE),
            );

            $this->Subkelas_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('subkelas'));
        }
    }

    public function update($id) {
        $row = $this->Subkelas_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('subkelas/update_action'),
                'sub_id' => set_value('sub_id', $row->sub_id),
                'sub_nama' => set_value('sub_nama', $row->sub_nama),
                'kelas_id' => set_value('kelas_id', $row->kelas_id),
            );
            $this->template->load('template', 'subkelas_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('subkelas'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('sub_id', TRUE));
        } else {
            $data = array(
                'sub_nama' => $this->input->post('sub_nama', TRUE),
                'kelas_id' => $this->input->post('kelas_id', TRUE),
            );

            $this->Subkelas_model->update($this->input->post('sub_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('subkelas'));
        }
    }

    public function delete($id) {
        $row = $this->Subkelas_model->get_by_id($id);

        if ($row) {
            $this->Subkelas_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('subkelas'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('subkelas'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('sub_nama', 'sub nama', 'trim|required');
        $this->form_validation->set_rules('kelas_id', 'kelas id', 'trim|required');

        $this->form_validation->set_rules('sub_id', 'sub_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "subkelas.xls";
        $judul = "subkelas";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Sub Nama");
        xlsWriteLabel($tablehead, $kolomhead++, "Kelas Id");

        foreach ($this->Subkelas_model->get_all_query() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->sub_nama);
            xlsWriteNumber($tablebody, $kolombody++, $data->kelas_nama);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=subkelas.doc");

        $data = array(
            'subkelas_data' => $this->Subkelas_model->get_all_query(),
            'start' => 0
        );

        $this->load->view('subkelas_doc', $data);
    }

    function pdf() {
        $data = array(
            'subkelas_data' => $this->Subkelas_model->get_all_query(),
            'start' => 0
        );

        $this->load->library('pdf');


        $html = $this->load->view('subkelas_doc', $data, true);

        $this->pdf->generate($html, 'subkelas');
        ;
    }

}

/* End of file Subkelas.php */
/* Location: ./application/controllers/Subkelas.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-06-17 04:33:19 */
/* http://harviacode.com */