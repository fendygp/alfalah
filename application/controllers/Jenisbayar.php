<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenisbayar extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Jenisbayar_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $jenisbayar = $this->Jenisbayar_model->get_all();

        $data = array(
            'jenisbayar_data' => $jenisbayar
        );

        $this->template->load('template', 'jenisbayar_list', $data);
    }

    public function read($id) {
        $row = $this->Jenisbayar_model->get_by_id($id);
        if ($row) {
            $data = array(
                'jenisbayar_id' => $row->jenisbayar_id,
                'jenisbayar_ket' => $row->jenisbayar_ket,
            );
            $this->template->load('template', 'jenisbayar_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jenisbayar'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('jenisbayar/create_action'),
            'jenisbayar_id' => set_value('jenisbayar_id'),
            'jenisbayar_ket' => set_value('jenisbayar_ket'),
        );
        $this->template->load('template', 'jenisbayar_form', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'jenisbayar_ket' => $this->input->post('jenisbayar_ket', TRUE),
            );

            $this->Jenisbayar_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('jenisbayar'));
        }
    }

    public function update($id) {
        $row = $this->Jenisbayar_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('jenisbayar/update_action'),
                'jenisbayar_id' => set_value('jenisbayar_id', $row->jenisbayar_id),
                'jenisbayar_ket' => set_value('jenisbayar_ket', $row->jenisbayar_ket),
            );
            $this->template->load('template', 'jenisbayar_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jenisbayar'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('jenisbayar_id', TRUE));
        } else {
            $data = array(
                'jenisbayar_ket' => $this->input->post('jenisbayar_ket', TRUE),
            );

            $this->Jenisbayar_model->update($this->input->post('jenisbayar_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('jenisbayar'));
        }
    }

    public function delete($id) {
        $row = $this->Jenisbayar_model->get_by_id($id);

        if ($row) {
            $this->Jenisbayar_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('jenisbayar'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jenisbayar'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('jenisbayar_ket', 'jenisbayar ket', 'trim|required');

        $this->form_validation->set_rules('jenisbayar_id', 'jenisbayar_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "jenisbayar.xls";
        $judul = "jenisbayar";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Jenisbayar Ket");

        foreach ($this->Jenisbayar_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->jenisbayar_ket);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=jenisbayar.doc");

        $data = array(
            'jenisbayar_data' => $this->Jenisbayar_model->get_all(),
            'start' => 0
        );

        $this->load->view('jenisbayar_doc', $data);
    }

    function pdf() {
        $data = array(
            'jenisbayar_data' => $this->Jenisbayar_model->get_all(),
            'start' => 0
        );



        $this->load->library('pdf');


        $html = $this->load->view('jenisbayar_doc', $data, true);

        $this->pdf->generate($html, 'siswa');
    }

}

/* End of file Jenisbayar.php */
/* Location: ./application/controllers/Jenisbayar.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-06-29 10:51:53 */
/* http://harviacode.com */