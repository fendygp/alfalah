<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kelas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Kelas_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $kelas = $this->Kelas_model->get_all();

        $data = array(
            'kelas_data' => $kelas
        );

        $this->template->load('template', 'kelas_list', $data);
    }

    public function read($id) {
        $row = $this->Kelas_model->get_by_id($id);
        if ($row) {
            $data = array(
                'kelas_id' => $row->kelas_id,
                'kelas_nama' => $row->kelas_nama,
            );
            $this->template->load('template', 'kelas_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kelas'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('kelas/create_action'),
            'kelas_id' => set_value('kelas_id'),
            'kelas_nama' => set_value('kelas_nama'),
        );
        $this->template->load('template', 'kelas_form', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'kelas_nama' => $this->input->post('kelas_nama', TRUE),
            );

            $this->Kelas_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('kelas'));
        }
    }

    public function update($id) {
        $row = $this->Kelas_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('kelas/update_action'),
                'kelas_id' => set_value('kelas_id', $row->kelas_id),
                'kelas_nama' => set_value('kelas_nama', $row->kelas_nama),
            );
            $this->template->load('template', 'kelas_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kelas'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kelas_id', TRUE));
        } else {
            $data = array(
                'kelas_nama' => $this->input->post('kelas_nama', TRUE),
            );

            $this->Kelas_model->update($this->input->post('kelas_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('kelas'));
        }
    }

    public function delete($id) {
        $row = $this->Kelas_model->get_by_id($id);

        if ($row) {
            $this->Kelas_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('kelas'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kelas'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('kelas_nama', 'kelas nama', 'trim|required');

        $this->form_validation->set_rules('kelas_id', 'kelas_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "kelas.xls";
        $judul = "kelas";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Kelas Nama");

        foreach ($this->Kelas_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->kelas_nama);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word() {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=kelas.doc");

        $data = array(
            'kelas_data' => $this->Kelas_model->get_all(),
            'start' => 0
        );

        $this->load->view('kelas_doc', $data);
    }

    function pdf() {
        $this->load->library('pdf');

        $data = array(
            'kelas_data' => $this->Kelas_model->get_all(),
            'start' => 0
        );

        $html = $this->load->view('kelas_doc', $data, true);

        $this->pdf->generate($html, 'siswa');
    }

}

/* End of file Kelas.php */
/* Location: ./application/controllers/Kelas.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-04-12 09:44:01 */
/* http://harviacode.com */