
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-4'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>BIAYA SEKOLAH
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="<?php echo $action; ?>" method="post">

                            <div class="form-group">
                                <label>Tahun Akademik</label>
                                <select class="form-control" name="tahun_id">
                                    <?php
                                    foreach ($tahun_akademik_data as $tahun_akademik) {

                                        if ($tahun_akademik->status == "aktif") {
                                            echo "<option value=" . $tahun_akademik->tahun_id . ">$tahun_akademik->tahun_ket</option>";
                                        }else{
                                            echo "<option value=" . $tahun_akademik->tahun_id . " hidden>$tahun_akademik->tahun_ket</option>";
                                        }
                                    }
                                    ?>

                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Setup</button>

                        </form>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-xs-8'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>
                        <?php echo anchor(site_url('biaya_sekolah/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('biaya_sekolah/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('biaya_sekolah/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <table class="table table-bordered table-striped" id="mytable">
                        <thead>
                            <tr>
                                <th width="80px">No</th>
                                <th>Jenis Bayar</th>
                                <th>Kelas</th>
                                <th>Tahun Akademik</th>
                                <th>Biaya Sekolah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $start = 0;
                            foreach ($biaya_sekolah_data as $biaya_sekolah) {
                                ?>
                                <tr>
                                    <td><?php echo ++$start ?></td>
                                    <td><?php echo $biaya_sekolah->jenisbayar_ket ?></td>
                                    <td><?php echo $biaya_sekolah->kelas_nama ?></td>
                                    <td><?php echo $biaya_sekolah->tahun_ket ?></td>
                                    <td><?php echo number_format($biaya_sekolah->biayasekolah_jumlah, 0, ",", ",") ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#mytable").dataTable();
                        });
                    </script>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->


</section><!-- /.content -->
