

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>

                    <h3 class='box-title'>Biaya Sekolah</h3>
                    <div class='box box-primary'>
                        <form action="<?php echo $action; ?>" method="post">
                          
                               
                                <input type="hidden" class="form-control" name="jenisbayar_id" id="jenisbayar_id" placeholder="Jenisbayar Id" value="<?php echo $jenisbayar_id; ?>" />
                
                             <div class="form-group">
                                <label for="int">Jenisbayar <?php echo form_error('jenisbayar_id') ?></label>
                                <input type="text" class="form-control" name="jenisbayar_ket" id="jenisbayar_id" placeholder="Jenisbayar Id" value="<?php echo $jenisbayar_ket; ?>" readonly/>
                            </div>
                            <div class="form-group">
                                <label for="int">Tahunakademik<?php echo form_error('tahunakademik_id') ?></label>
                                <input type="text" class="form-control" name="tahunakademik_id" id="tahunakademik_id" placeholder="Tahunakademik Id" value="<?php echo $tahunakademik_ket; ?>" readonly/>
                            </div>
                         
                               
                                <input type="hidden" class="form-control" name="tahunakademik_id" id="tahunakademik_id" placeholder="Tahunakademik Id" value="<?php echo $tahunakademik_id; ?>" />
                      
                            <div class="form-group">
                                <label for="int">Biayasekolah Jumlah <?php echo form_error('biayasekolah_jumlah') ?></label>
                                <input type="text" class="form-control" name="biayasekolah_jumlah" id="biayasekolah_jumlah" placeholder="Biayasekolah Jumlah" value="<?php echo $biayasekolah_jumlah; ?>" />
                            </div>
                            <input type="hidden" name="biayasekolah_id" value="<?php echo $biayasekolah_id; ?>" />
                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                            <a href="<?php echo site_url('biaya_sekolah') ?>" class="btn btn-default">Cancel</a>
                        </form>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->

<script src="<?php echo base_url() ?>assets2/jquery-1.11.0.js"></script>

<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script src="<?php echo base_url(); ?>assets2/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets2/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets2/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>

<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
    $('.datepicker').datetimepicker({
        language: 'id',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
</script>
