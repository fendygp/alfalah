
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>

                    <h3 class='box-title'>HAK AKSES</h3>
                    <div class='box box-primary'>

                        <form action="<?php echo $action; ?>" method="post">

                            <div class="form-group">
                                <label>User</label>
                                <select class="form-control"  name="user_id">
                                    <?php
                                    foreach ($user_data as $ud) {

                                        if($user_id == $ud->id){
                                            echo "<option value=" . $ud->id . " selected>$ud->username</option>"; 
                                        }else{
                                        
                                        echo "<option value=" . $ud->id . ">$ud->username</option>";
                                        }
                                    }
                                    ?>

                                </select>
                            </div>

                             <div class="form-group">
                                <label>Group</label>
                                <select class="form-control"  name="group_id">
                                    <?php
                                    foreach ($group_data as $gd) {

                                        if($group_id == $gd->id){
                                            echo "<option value=" . $gd->id . " selected>$gd->name</option>"; 
                                        }else{
                                        
                                        echo "<option value=" . $gd->id . ">$gd->name</option>";
                                        }
                                    }
                                    ?>

                                </select>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                            <a href="<?php echo site_url('users_groups') ?>" class="btn btn-default">Cancel</a>
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->

<script src="<?php echo base_url() ?>assets2/jquery-1.11.0.js"></script>

<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script src="<?php echo base_url(); ?>assets2/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets2/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets2/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>

<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
    $('.datepicker').datetimepicker({
        language: 'id',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
</script>
