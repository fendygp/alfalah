
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Data Persen dalam:
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="#" method="post">

                            <div class="form-group">
                                <select class="form-control" name="jenis_lapor" disabled>
                                    <option value="harian">Harian</option>
                                    <option value="bulanan">Bulanan</option>
                                    <option value="tahunan">Tahunan</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary" disabled>Setup</button>
                            <a href="<?php echo site_url('Persenbayar') ?>" class="btn btn-default">Reset</a>
                        </form>

                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="<?php echo $action; ?>" method="post">


                            <div class="form-group">
                                <label>Tahun</label>
                                <select class="form-control" name="tahunawal">
                                    <?php foreach ($tahundata as $tahunakademik) { ?>
                                        <?php echo "<option value='$tahunakademik->tahun_ket'>$tahunakademik->tahun_ket</option>" ?>

                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tahun</label>
                                <select class="form-control" name="tahunakhir">
                                    <?php foreach ($tahundata as $tahunakademik) { ?>
                                        <?php echo "<option value='$tahunakademik->tahun_ket'>$tahunakademik->tahun_ket</option>" ?>

                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>


                            <button type="submit" class="btn btn-primary">Lihat</button>

                        </form>

                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->


    </div><!-- /.row -->
    <div class='row'>

        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <!--                    <h3 class='box-title'>
                    <?php echo anchor(site_url('Laporankeu/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
                    <?php echo anchor(site_url('Laporankeu/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
                    <?php echo anchor(site_url('Laporankeu/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>-->
                </div><!-- /.box-header -->
                <div class='box-body'>

                    <div class="box-header with-border">
                        <h3 class="box-title">Jumlah Data Keuangan <?php
                            echo $tahunawal . " - " . $tahunakhir;
                            $judul = "Jumlah Data Keuangan" . $tahunawal . " - " . $tahunakhir;
                            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 

                        </h3>


                    </div>

                    <div class="box-body chart-responsive">
                        <div class="chart" id="bar-chart" style="height: 300px;"></div>
                    </div>
                    <!-- /.box-body -->
                </div>


            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div><!-- /.col -->


    <script src="<?php echo base_url('assets2/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
    <script src="<?php echo base_url('assets2/js/raphael-min.js') ?>"></script>

    <script src="<?php echo base_url('assets2/js/highcharts.js') ?>"></script>
    <script src="<?php echo base_url('assets2/js/modules/data.js') ?>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#mytable").dataTable();
        });
    </script>

    <script type="text/javascript">

        //RANDOM COLOR
        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }




        //HIGHCHART BAR
        $(document).ready(function () {
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'bar-chart',
                    type: 'column',
                    margin: [50, 50, 100, 80]
                },
                title: {
                    text: '<?php echo $judul; ?>'
                },
                xAxis: {
                    type: 'category'

                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total (Rupiah)'
                    }
                },
                legend: {
                    enabled: false
                },
                series: [{
                        name: 'Total',
                        data: [
<?php foreach ($lapordata as $lp) { ?>
                                {
                                    name: '<?php echo (string) $lp->tahun; ?>',
                                    y: <?php echo $lp->total; ?>,
                                    color: getRandomColor()

                                },
<?php } ?>
                        ]
                    }]
            });


        });
    </script>
</section><!-- /.content -->

