

<!-- Main content -->
<section class='content'>

    <div class='row'>

        <div class='col-xs-4'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>BIAYA SEKOLAH
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">

                        <div class="form-group">
                            <label>Tahun Akademik</label>
                            <select class="form-control" name="tahun_id" disabled>

                                <?php
                                foreach ($tahun_ket as $tahun_akademik) {

                                    echo "<option value=" . $tahun_akademik->tahun_id . ">$tahun_akademik->tahun_ket</option>";
                                }
                                ?>

                            </select>
                        </div>

                        <div class="form-group">

                            <label>Kelas</label>
                            <select class="form-control" name="kelas_id" disabled>
                                <?php
                                foreach ($kelas_data as $kelas) {

                                    echo "<option value=" . $kelas->kelas_id . ">$kelas->kelas_nama</option>";
                                }
                                ?>

                            </select>
                        </div>
                        <a href="<?php echo site_url('biaya_sekolah') ?>" class="btn btn-default">back</a>

                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-xs-8'>
            <div class='box'>
                <div class='box-header'>
                    <!--                        <h3 class='box-title'> 
                    <?php echo anchor(site_url('biaya_sekolah/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
                    <?php echo anchor(site_url('biaya_sekolah/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
                    <?php echo anchor(site_url('biaya_sekolah/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>-->
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <form action="<?php echo $action; ?>" method="post">
                        <table class="table table-bordered table-striped" id="mytable">
                            <thead>
                                <tr>
                                    <th width="80px">No</th>
                                    <th>Jenis bayar</th>

                                    <th>Biaya sekolah</th>
                          

                                </tr>

                            </thead>

                            <tbody>

                                <?php
                                $start = 0;
                                foreach ($jenisbayar_data as $jenisbayar) {
                                    ?>

                                    <tr>

                                        <td><?php echo ++$start ?></td>
                                        <td><?php echo $jenisbayar->jenisbayar_ket ?></td>
                                        <?php
                                        
//                                        foreach ($biaya_sekolah_data as $biaya_sekolah) {
//                                            if ($jenisbayar->jenisbayar_id == $biaya_sekolah->jenisbayar_id) {
//                                                $jumlah[$start] = $biaya_sekolah->biayasekolah_jumlah;
//                                            } else {
//                                                $jumlah[$start] = 0;
//                                            }
//                                        }
                                        ?>

                                        <td>
                                            <input type="text" class="form-control" name="<?php echo 'jumlah_bayar_' . $jenisbayar->jenisbayar_id; ?>" placeholder="Jumlah Bayar" />

                                            <input type="hidden" class="form-control" name="<?php echo 'jenisbayar_id_' . $jenisbayar->jenisbayar_id; ?>" value="<?php echo $jenisbayar->jenisbayar_id; ?>"/>
                                            <input type="hidden" class="form-control" name="<?php echo 'tahun_id_' . $jenisbayar->jenisbayar_id; ?>" value="<?php echo $tahun_id; ?>"/>
                                            <input type="hidden" class="form-control" name="<?php echo 'kelas_id_' . $jenisbayar->jenisbayar_id; ?>" value="<?php echo $kelas->kelas_id; ?>"/>

                                     
            <!--                                               <input type="submit" 
             style="position: absolute; left: -9999px; width: 1px; height: 1px;"
             tabindex="-1" />-->
                                        </td>

                                    </tr>


                                    <?php
                                }
                                ?>
                            </tbody>


                        </table>
                        <button type="submit" class="btn btn-primary">Input</button>
                    </form>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<!--                        <script type="text/javascript">
                        $(document).ready(function () {
                            $("#mytable").dataTable();
                        });
                    </script>-->
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    <div class='row'>
        <div class='col-xs-12'>
              <!--<a href="<?php echo site_url('biaya_sekolah') ?>" class="btn btn-default">Cancel</a>-->

        </div>
    </div>





</section><!-- /.content -->
