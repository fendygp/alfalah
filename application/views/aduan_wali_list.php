
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Data Aduan Wali</h3>
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <table class="table table-bordered table-striped" id="mytable">
                        <thead>
                            <tr>

                                <th>No</th>
                                <th>Nama Wali</th>
                                <th>Nama Siswa</th>
                                <th>Jenis Pembayaran</th>
                                <th>Link</th>
                                <th>Action</th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $a = 0;
                            foreach ($aduan_data as $b) {
                                ?>
                                <tr>
                                    <td><?php echo ++$a; ?></td>
                                    <td><?php echo $b->username; ?></td>
                                    <td><?php echo $b->siswa_nama; ?></td>
                                    <td><?php echo $b->keterangan; ?></td>
                                    <td><a href="<?php echo $b->link_photo; ?>" target="_blank">Photo</a></td>
                                    <td style="text-align:center" width="140px">
    <?php
    echo anchor(site_url('Aduan_wali/bayarsiswa/' . $b->siswa_nis), '<i>Konfrimasi Pembayaran</i>', array('title' => 'detail', 'class' => 'btn btn-danger btn-sm'));
  
    ?>
                                    </td>


                                </tr>
    <?php
}
?>
                        </tbody>
                    </table>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#mytable").dataTable();
                        });
                    </script>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->


</section><!-- /.content -->
