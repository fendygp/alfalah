
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                <h3 class='box-title'>Siswa Read</h3>
        <table class="table table-bordered">
	    <tr><td>NIS</td><td><?php echo $siswa_nis; ?></td></tr>
	    <tr><td>Kelas</td><td><?php echo $kelas_nama; echo $sub_nama ?></td></tr>
	    <tr><td>Tahun Akademik</td><td><?php echo $tahun_id; ?></td></tr>
	    <tr><td>Nama</td><td><?php echo $siswa_nama; ?></td></tr>
	    <tr><td>Jenis Kelamin</td><td><?php echo $siswa_jk; ?></td></tr>
	    <tr><td>Tempat Lahir</td><td><?php echo $siswa_tempatlahir; ?></td></tr>
	    <tr><td>Tanggal Lahir</td><td><?php echo $siswa_tgllahir; ?></td></tr>
	    <tr><td>Alamat</td><td><?php echo $siswa_alamat; ?></td></tr>
	    <tr><td>Nama Ayah</td><td><?php echo $siswa_ayah; ?></td></tr>
	    <tr><td>Nama Ibu</td><td><?php echo $siswa_ibu; ?></td></tr>
	    <tr><td>Pekerjaan Ayah</td><td><?php echo $siswa_ayahkerja; ?></td></tr>
	    <tr><td>Pekerjaan Ibu</td><td><?php echo $siswa_ibukerja; ?></td></tr>
	    <tr><td>Alamat Orang Tua</td><td><?php echo $siswa_alamatortu; ?></td></tr>
            <tr><td>User Wali</td><td><?php echo $user_wali; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('siswa') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->