
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>BIODATA SISWA</h3>
                </div><!-- /.box-header -->
                <div class='box-body'>
                 
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>

                                <th>NIS</th>
                                <th>Kelas</th>
                                <th>Tahun Akademik</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Alamat</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($siswa_data as $siswa) {
                                $siswa_id = $siswa->siswa_id;
                                
                                ?>
                                <tr>

                                    <td><?php echo $siswa->siswa_nis ?></td>
                                    <td><?php echo $siswa->kelas_nama . " " . $siswa->sub_nama ?></td>
                                    <td><?php echo $siswa->tahun_ket ?></td>
                                    <td><?php echo $siswa->siswa_nama ?></td>
                                    <td><?php echo $siswa->siswa_jk ?></td>
                                    <td><?php echo $siswa->siswa_tempatlahir ?></td>
                                    <td><?php echo $siswa->siswa_tgllahir ?></td>
                                    <td><?php echo $siswa->siswa_alamat ?></td>

                                </tr>
    <?php
}
?>
                        </tbody>
                    </table>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#mytable").dataTable();
                        });
                    </script>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
	
    <div class='row'>

        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>
                        
                        RINCIAN PEMBAYARAN
                        <?php echo anchor(site_url('biaya_sekolah/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('biaya_sekolah/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('biaya_sekolah/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>
                </div>
                <div class='box-body'>
                    <table class="table table-bordered table-striped" id="mytable2">
                        <thead>
                            <tr>
                                <!--<th width="80px">No</th>-->
                                <th>Jenis pembayaran</th>
                                <th>Jumlah Tagihan</th>
                                <th>Sudah dibayar</th>
                                <th>Belum dibayar</th>
                                <th>Keteranngan</th>
                   
<!--                                <th>Action</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
//                            $a = 0;

                            foreach ($pembayaransiswa_data as $yangdibayar) {
                                ?>
                                <tr>
                                    <!--<td><?php echo ++$a; ?></td>-->
                                    <td><?php echo $yangdibayar->jenisbayar_ket; ?></td>
                                    <td><?php echo "Rp " . number_format($yangdibayar->biayasekolah_jumlah, 0); ?></td>
                                    <td><?php
                                        $biayasekolah_id = $yangdibayar->biayasekolah_id;
                                        $dibayar2 = $yangdibayar->bayar_jumlah;

                                        $hasil = $yangdibayar->biayasekolah_jumlah - $dibayar2;
                                        $pembayaran_id = $yangdibayar->pembayaran_id;
                                        echo "Rp " . number_format($yangdibayar->bayar_jumlah, 0);
                                        ?></td>

                                    <td><?php echo "Rp " . number_format($hasil,0); ?></td>
                                    <td>
									<?php
                                    if ($hasil == 0) {
                                        echo 'Lunas';
                                    } else {
                                        echo 'Belum Lunas';
                                    }
                                    ?>
									</td>
                                   
    <!--                                    <td style="text-align:center" width="140px">
                                    <?php
                                    if ($dibayar2 == 0) {
                                        echo 'no action';
                                    } else {
                                        echo anchor(site_url('pembayaran/update/' . $pembayaran_id), '<i class="fa fa-pencil-square-o"></i>', array('title' => 'edit', 'class' => 'btn btn-danger btn-sm'));
                                        echo '  ';
                                        echo anchor(site_url('pembayaran/delete/' . $pembayaran_id), '<i class="fa fa-trash-o"></i>', 'title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                                    }
                                    ?>
                                    </td>-->
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#mytable2").dataTable();
                        });
                    </script>


                </div> 
            </div> 
        </div> 
    </div> 
    <div class='row'>

        <div class='col-xs-8'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>
                       DETAIL PEMBAYARAN</h3>
                </div>
                <div class='box-body'>
                    <table class="table table-bordered table-striped" id="mytable">
                        <thead>
                            <tr>
                                <!--<th width="80px">No</th>-->
                                <th>Jenis pembayaran</th>
                             
                                <th>Jumlah bayar</th>
                            
                                <th>Tanggal Bayar</th>
<!--                                <th>Action</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
//                            $a = 0;

                            foreach ($pembayaransiswa_data2 as $yangdibayar2) {
                                ?>
                                <tr>
                                    <!--<td><?php echo ++$a; ?></td>-->
                                    <td><?php echo $yangdibayar2->jenisbayar_ket; ?></td>
                                   
                                    <td><?php
                                        $biayasekolah_id = $yangdibayar2->biayasekolah_id;
                                        $dibayar2 = $yangdibayar2->pembayaran_jumlah;

                                    
                                        $pembayaran_id = $yangdibayar->pembayaran_id;
                                        echo "Rp " . number_format($dibayar2, 0);
                                        ?></td>

                                    
                                    <td><?php
                                        $tanggal = $yangdibayar2->pembayaran_tanggal;


                                        $tahun = '';
                                        $tgl = '';
                                        $bln = '';
                                        $thn = '';

                                        for ($i = 0; $i < strlen($tanggal); $i++) {
                                            if ($tanggal[$i] != "-") {
                                                $tahun = $tahun . $tanggal[$i];
                                            }
                                        }

                                        for ($i = 0; $i < 4; $i++) {
                                            $thn = $thn . $tahun[$i];
                                        }
                                        for ($i = 4; $i < 6; $i++) {
                                            $bln = $bln . $tahun[$i];
                                        }
                                        switch ($bln) {
                                            case 1:
                                                $namabulan = "Januari";
                                                break;
                                            case 2:
                                                $namabulan = "Februari";
                                                break;
                                            case 3:
                                                $namabulan = "Maret";
                                                break;
                                            case 4:
                                                $namabulan = "April";
                                                break;
                                            case 5:
                                                $namabulan = "Mei";
                                                break;
                                            case 6:
                                                $namabulan = "Juni";
                                                break;
                                            case 7:
                                                $namabulan = "Juli";
                                                break;
                                            case 8:
                                                $namabulan = "Agustus";
                                                break;
                                            case 9:
                                                $namabulan = "September";
                                                break;
                                            case 10:
                                                $namabulan = "Oktober";
                                                break;
                                            case 11:
                                                $namabulan = "November";
                                                break;
                                            case 12:
                                                $namabulan = "Desember";
                                                break;
                                        }
                                        for ($i = 6; $i < strlen($tahun); $i++) {
                                            $tgl = $tgl . $tahun[$i];
                                        }
                                        $tabel = $tgl . "&nbsp;" . $namabulan . "&nbsp;" . $thn;

                                        echo $tabel;
                                        ?></td>
    <!--                                    <td style="text-align:center" width="140px">
                                    <?php
                                    if ($dibayar2 == 0) {
                                        echo 'no action';
                                    } else {
                                        echo anchor(site_url('pembayaran/update/' . $pembayaran_id), '<i class="fa fa-pencil-square-o"></i>', array('title' => 'edit', 'class' => 'btn btn-danger btn-sm'));
                                        echo '  ';
                                        echo anchor(site_url('pembayaran/delete/' . $pembayaran_id), '<i class="fa fa-trash-o"></i>', 'title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                                    }
                                    ?>
                                    </td>-->
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#mytable").dataTable();
                        });
                    </script>


                </div> 
            </div> 
        </div> 
    </div>
</section><!-- /.content -->
