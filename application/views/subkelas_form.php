<!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                
                  <h3 class='box-title'>SUBKELAS</h3>
                      <div class='box box-primary'>
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Sub Kelas <?php echo form_error('sub_nama') ?></td>
            <td>
			<select name="sub_nama" class="form-control">
                <option value="<?php echo $sub_nama; ?>"><?php echo $sub_nama; ?></option>
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
                <option value="E">E</option>                
            </select>
        </td>
	    <tr><td>Kelas<?php echo form_error('kelas_id') ?></td>
            <td><?php echo cmb_dinamis('kelas_id', 'kelas', 'kelas_nama', 'kelas_id', $kelas_id) ?>
        </td>
	    <input type="hidden" name="sub_id" value="<?php echo $sub_id; ?>" /> 
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('subkelas') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table></form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->