
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Data Persen dalam:
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="<?php echo $action; ?>" method="post">
                            
                            
                            <div class="form-group">
                            <select class="form-control" name="jenis_persen">
                                <option value="harian">Harian</option>
                                <option value="bulanan">Bulanan</option>
                                <option value="tahunan">Tahunan</option>
                            </select>
                            </div>


                            <button type="submit" class="btn btn-primary">Setup</button>
                            <a href="<?php echo site_url('laporankeu') ?>" class="btn btn-default">Reset</a>
                        </form>

                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->


    </div><!-- /.row -->


</section><!-- /.content -->

