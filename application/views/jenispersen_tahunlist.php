
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Jenis Laporan
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="#" method="post">

                            <div class="form-group">
                                <select class="form-control" name="jenis_lapor" disabled>
                                    <option value="harian">Harian</option>
                                    <option value="bulanan">Bulanan</option>
                                    <option value="tahunan">Tahunan</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary" disabled>Setup</button>
                            <a href="<?php echo site_url('laporankeu') ?>" class="btn btn-default">Reset</a>
                        </form>

                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="<?php echo $action; ?>" method="post">


                            <div class="form-group">
                                <label>Tahun awal</label>
                                <select class="form-control" name="tahunawal">
                                    <?php foreach ($tahundata as $tahunakademik) { ?>
                                      <?php  echo "<option value='$tahunakademik->tahun_ket'>$tahunakademik->tahun_ket</option>" ?>
                                     
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Tahun awal</label>
                                <select class="form-control" name="tahunakhir">
                                    <?php foreach ($tahundata as $tahunakademik) { ?>
                                      <?php  echo "<option value='$tahunakademik->tahun_ket'>$tahunakademik->tahun_ket</option>" ?>
                                     
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>


                            <button type="submit" class="btn btn-primary">Lihat</button>

                        </form>

                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->


    </div><!-- /.row -->
    <div class='row'>

        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
<!--                    <h3 class='box-title'>
                        <?php echo anchor(site_url('Laporankeu/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('Laporankeu/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('Laporankeu/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>-->
                </div><!-- /.box-header -->
                <div class='box-body'>

                    <div class="box-header with-border">
                        <h3 class="box-title">Jumlah data Keuangan</h3>
						<?php echo $tahunawal . " - " . $tahunakhir; ?>


                    </div>
                    <?php
                    echo "<p>Jumlah belum dibayar: Rp. $belumdibayar</p>";

                    echo "<p>Jumlah total masuk: Rp. $totalmasuk</p>";

                    echo "<p>Jumlah total anggaran: Rp. $totalkredit</p>";


                    $persenbelumbayar = $belumdibayar / $totalkredit * 100;
                    $persenbayar = $totalmasuk / $totalkredit * 100;
                    ?>
                    <div class="box-body chart-responsive">
                        <div class="chart" id="sales-chart1" style="height: 300px; position: relative;"></div>
                    </div>
                    <!-- /.box-body -->
                </div>


            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url('assets2/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets2/js/raphael-min.js') ?>"></script>
<script src="<?php echo base_url('assets2/AdminLTE-2.0.5/plugins/morris/morris.min.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#mytable").dataTable();
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        "use strict";

        //DONUT CHART
        var donut = new Morris.Donut({
            element: 'sales-chart1',
            resize: true,
            colors: ["#3c8dbc", "#f56954"],
            data: [
                {label: "Belum Bayar", value: <?php echo number_format($persenbelumbayar, 2); ?>},
                {label: "Sudah Bayar", value: <?php echo number_format($persenbayar, 2); ?>}

            ],
            hideHover: 'auto'
        });
        //BAR CHART

    });
</script>
</section><!-- /.content -->
