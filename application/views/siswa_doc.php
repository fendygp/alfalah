<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Siswa List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>NIS</th>
		<th>Kelas</th>
		<th>Tahun Akademik</th>
		<th>Nama</th>
		<th>Jenis Kelamin</th>
		<th>Tempat Lahir</th>
		<th>Tanggal Lahir</th>
		<th>Alamat</th>
		<th>Nama Ayah</th>
		<th>Nama Ibu</th>
		<th>Pekerjaan Ayah</th>
		<th>Pekerjaan Ibu</th>
		<th>Alamat Orang Tua</th>
                <th>User wali</th>
		
            </tr><?php
            foreach ($siswa_data as $siswa)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $siswa->siswa_nis ?></td>
		      <td><?php echo $siswa->kelas_nama." ".$siswa->sub_nama ?></td>
		      <td><?php echo $siswa->tahun_ket ?></td>
		      <td><?php echo $siswa->siswa_nama ?></td>
		      <td><?php echo $siswa->siswa_jk ?></td>
		      <td><?php echo $siswa->siswa_tempatlahir ?></td>
		      <td><?php echo $siswa->siswa_tgllahir ?></td>
		      <td><?php echo $siswa->siswa_alamat ?></td>
		      <td><?php echo $siswa->siswa_ayah ?></td>
		      <td><?php echo $siswa->siswa_ibu ?></td>
		      <td><?php echo $siswa->siswa_ayahkerja ?></td>
		      <td><?php echo $siswa->siswa_ibukerja ?></td>
		      <td><?php echo $siswa->siswa_alamatortu ?></td>	
                       <td><?php echo $siswa->username ?></td>	
                      
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>