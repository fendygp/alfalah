<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Biaya_sekolah List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Jenisbayar Id</th>
		<th>Tahunakademik Id</th>
		<th>Biayasekolah Jumlah</th>
		
            </tr><?php
            foreach ($biaya_sekolah_data as $biaya_sekolah)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $biaya_sekolah->jenisbayar_id ?></td>
		      <td><?php echo $biaya_sekolah->tahunakademik_id ?></td>
		      <td><?php echo $biaya_sekolah->biayasekolah_jumlah ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>