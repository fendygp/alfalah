
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Data Mahasiswa
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label>Nomor Induk Siswa (NIS)</label>
                                <input type="text" name="nis" class="form-control" placeholder="Enter NIS">
                            </div>


                            <button type="submit" class="btn btn-primary">Search</button>
                            <a href="<?php echo site_url('pembayaran') ?>" class="btn btn-default">Reset</a>
                        </form>

                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Form Pembayaran
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="#" method="post">
                            <div class="form-group">
                                <label>Jenis Pembayaran</label>
                                <select class="form-control"  name="biayasekolah_id">
                                    <?php
                                    foreach ($yangdibayar_data as $jb) {

                                        echo "<option value=" . $jb->biayasekolah_id . ">$jb->jenisbayar_ket</option>";
                                    }
                                    ?>

                                </select>
                            </div>


                           

                            <button type="submit" class="btn btn-primary" >Submit</button>

                        </form>

                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->


</section><!-- /.content -->

