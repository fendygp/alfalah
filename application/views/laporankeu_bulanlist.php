
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Jenis Laporan
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="#" method="post">

                            <div class="form-group">
                                <select class="form-control" name="jenis_lapor" disabled>
                                    <option value="harian">Harian</option>
                                    <option value="bulanan">Bulanan</option>
                                    <option value="tahunan">Tahunan</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary" disabled>Setup</button>
                            <a href="<?php echo site_url('laporankeu') ?>" class="btn btn-default">Reset</a>
                        </form>

                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>

                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="<?php echo $action; ?>" method="post">

                            <div class="form-group">
                                <label>Bulan</label>
                                <select class="form-control" name="bulanawal">

                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Bulan</label>
                                <select class="form-control" name="bulanakhir">

                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>

                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Lihat</button>

                        </form>

                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->


    </div><!-- /.row -->
    <div class='row'>

        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>
                        LAPORAN BULAN &nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                        switch ($bulanawal) {
                            case 1:
                                $namabulan = "Januari";
                                break;
                            case 2:
                                $namabulan = "Februari";
                                break;
                            case 3:
                                $namabulan = "Maret";
                                break;
                            case 4:
                                $namabulan = "April";
                                break;
                            case 5:
                                $namabulan = "Mei";
                                break;
                            case 6:
                                $namabulan = "Juni";
                                break;
                            case 7:
                                $namabulan = "Juli";
                                break;
                            case 8:
                                $namabulan = "Agustus";
                                break;
                            case 9:
                                $namabulan = "September";
                                break;
                            case 10:
                                $namabulan = "Oktober";
                                break;
                            case 11:
                                $namabulan = "November";
                                break;
                            case 12:
                                $namabulan = "Desember";
                                break;
                        }

                        switch ($bulanakhir) {
                            case 1:
                                $namabulan2 = "Januari";
                                break;
                            case 2:
                                $namabulan2 = "Februari";
                                break;
                            case 3:
                                $namabulan2 = "Maret";
                                break;
                            case 4:
                                $namabulan2 = "April";
                                break;
                            case 5:
                                $namabulan2 = "Mei";
                                break;
                            case 6:
                                $namabulan2 = "Juni";
                                break;
                            case 7:
                                $namabulan2 = "Juli";
                                break;
                            case 8:
                                $namabulan2 = "Agustus";
                                break;
                            case 9:
                                $namabulan2 = "September";
                                break;
                            case 10:
                                $namabulan2 = "Oktober";
                                break;
                            case 11:
                                $namabulan2 = "November";
                                break;
                            case 12:
                                $namabulan2 = "Desember";
                                break;
                        }

                        echo $namabulan . " - " . $namabulan2;
                        ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo anchor(site_url('Laporankeu/excelbulan/' . $bulanawal . "/" . $bulanakhir), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('Laporankeu/wordbulan/' . $bulanawal . "/" . $bulanakhir), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('Laporankeu/pdfbulan/' . $bulanawal . "/" . $bulanakhir), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?>


                    </h3>
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <table class="table table-bordered table-striped" id="mytable">
                        <thead>
                            <tr>
                                <th width="80px">No</th>
                                <th>NIS</th>
                                <th>Nama</th>
                                <th>Jenis Bayar</th>
                                <th>Tanggal bayar</th>
                                <th>Jumlah Pemasukan</th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $a = 0;

                            foreach ($hariandata as $harian) {
                                ?>
                                <tr>
                                    <td><?php echo ++$a; ?></td>
                                    <td><?php echo $harian->siswa_nis; ?></td>
                                    <td><?php echo $harian->siswa_nama; ?></td>
                                    <td><?php echo $harian->jenisbayar_ket ?></td>
                                    <td><?php
                                        $tanggal = $harian->pembayaran_tanggal;


                                        $tahun = '';
                                        $tgl = '';
                                        $bln = '';
                                        $thn = '';

                                        for ($i = 0; $i < strlen($tanggal); $i++) {
                                            if ($tanggal[$i] != "-") {
                                                $tahun = $tahun . $tanggal[$i];
                                            }
                                        }

                                        for ($i = 0; $i < 4; $i++) {
                                            $thn = $thn . $tahun[$i];
                                        }
                                        for ($i = 4; $i < 6; $i++) {
                                            $bln = $bln . $tahun[$i];
                                        }
                                        switch ($bln) {
                                            case 1:
                                                $namabulan = "Januari";
                                                break;
                                            case 2:
                                                $namabulan = "Februari";
                                                break;
                                            case 3:
                                                $namabulan = "Maret";
                                                break;
                                            case 4:
                                                $namabulan = "April";
                                                break;
                                            case 5:
                                                $namabulan = "Mei";
                                                break;
                                            case 6:
                                                $namabulan = "Juni";
                                                break;
                                            case 7:
                                                $namabulan = "Juli";
                                                break;
                                            case 8:
                                                $namabulan = "Agustus";
                                                break;
                                            case 9:
                                                $namabulan = "September";
                                                break;
                                            case 10:
                                                $namabulan = "Oktober";
                                                break;
                                            case 11:
                                                $namabulan = "November";
                                                break;
                                            case 12:
                                                $namabulan = "Desember";
                                                break;
                                        }
                                        for ($i = 6; $i < strlen($tahun); $i++) {
                                            $tgl = $tgl . $tahun[$i];
                                        }
                                        $tabel = $tgl . "&nbsp;" . $namabulan . "&nbsp;" . $thn;

                                        echo $tabel;
                                        ?></td>
                                    <td><?php
                                        if ($harian->pembayaran_jumlah == NULL) {
                                            echo "0";
                                        } else {
                                            echo number_format($harian->pembayaran_jumlah, 0);
                                        }
                                        ?></td>

                                </tr>
                            <?php } ?>



                        </tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>TOTAL</strong></td>
                            <td><strong><?php
                                    echo number_format($total, 0);
                                    ?></strong></td>
                        </tr>
                    </table>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#mytable").dataTable();
                        });
                    </script>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

</section><!-- /.content -->

