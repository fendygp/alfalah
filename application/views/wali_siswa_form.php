

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>

                    <h3 class='box-title'>WALI SISWA</h3>
                    <div class='box box-primary'>
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label>User</label>
                                <select class="form-control"  name="user_id">
                                    <?php
                                    foreach ($user_data as $ud) {

                                        if($user_id == $ud->id){
                                            echo "<option value=" . $ud->id . " selected>$ud->username</option>"; 
                                        }else{
                                        
                                        echo "<option value=" . $ud->id . ">$ud->username</option>";
                                        }
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Siswa</label>
                                <select class="form-control"  name="siswa_id">
                                    <?php
                                    foreach ($siswa_data as $sd) {

                                        if($siswa_id == $sd->siswa_id){
                                            echo "<option value=" . $sd->siswa_id . " selected>$sd->siswa_nama</option>"; 
                                        }else{
                                        
                                        echo "<option value=" . $sd->siswa_id . ">$sd->siswa_nama</option>";
                                        }
                                    }
                                    ?>

                                </select>
                            </div>
                            <input type="hidden" name="wali_id" value="<?php echo $wali_id; ?>" /> 
                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                            <a href="<?php echo site_url('wali_siswa') ?>" class="btn btn-default">Cancel</a>
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->

