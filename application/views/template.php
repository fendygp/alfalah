<?php
if ($this->session->userdata('username') == '') {
    redirect('auth');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>FINANCE | SMP AL-FALAH AS-SALAM</title>
        <link href="<?php echo base_url() ?>template/dist/img/logomu.png" rel="SHORTCUT ICON"/>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo base_url() ?>template/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->
        <link rel="stylesheet" href="<?php echo base_url() ?>template/font-awesome-4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <!--<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->
        <link rel="stylesheet" href="<?php echo base_url() ?>template/ionicons-2.0.1/css/ionicons.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?php echo base_url() ?>template/plugins/datatables/dataTables.bootstrap.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url() ?>template/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url() ?>template/dist/css/skins/_all-skins.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="index2.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>CPANEL</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <?php if ($this->session->userdata('lvl')== 4){
                       
                                ?>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-warning"><?php foreach ($totalnotif as $tf){
                                        echo $tf->jumlahnotif;
                                    }
                                        ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                   
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            
                                            <?php foreach ($getnotif as $g){ ?>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-warning text-yellow"></i>
                                                    
                                                      <?php echo $g->jenisbayar_ket; ?> sudah dibayar
                                                </a>
                                            </li>
                                            <?php } ?>
                                            
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="<?php echo site_url('deletenotif/delete');?>">View all</a></li>
                                </ul>
                            </li>
                            <?php } else{
                                
                            }
?>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i><?php echo strtoupper($this->session->userdata('identity')); ?>                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url() ?>template/dist/img/user.png" class="img-circle" alt="User Image">
                                        <p>
                                            <?php echo strtoupper($this->session->userdata('username')); ?>
                                            <small><?php echo date('d-F-Y'); ?></small>
                                        </p>
                                    </li>
                                    <li class="user-footer no-padding">
                                        <?php
                                        echo anchor('dashboard/logout', 'Sign Out', array('class' => 'btn btn-default btn-flat'));
                                        ?>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>-->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url() ?>template/dist/img/user1.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo strtoupper($this->session->userdata('username')); ?></p>
                            <a><?php echo date('d-F-Y'); ?></a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="cari" id="input_menu_search" onkeyup="searchMenu()" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" id style="display: none">
                        <li></li>
                    </ul>
                    <ul class="sidebar-menu">
                        
                        <?php
                        $menu = $this->db->get_where('menu', array('is_parent' => 0, 'is_active' => 1, 'level' => $this->session->userdata('lvl')));
                        foreach ($menu->result() as $m) {
                            // chek ada sub menu
                            $submenu = $this->db->get_where('menu', array('is_parent' => $m->id, 'is_active' => 1));
                            if ($submenu->num_rows() > 0) {
                                // tampilkan submenu
                                echo "<li class='treeview menu_class' id='".strtolower($m->name)."'>
                                    " . anchor('#', "<i class='$m->icon'></i>" . strtoupper($m->name) . ' <i class="fa fa-angle-left pull-right"></i>') . "
                                        <ul class='treeview-menu'>";
                                foreach ($submenu->result() as $s) {
                                    echo "<li>" . anchor($s->link, "<i class='$s->icon'></i> <span>" . strtoupper($s->name)) . "</span></li>";
                                }
                                echo"</ul>
                                    </li>";
                            } else {
                                echo "<li class='menu_class' id='".strtolower($m->name)."'>" . anchor($m->link, "<i class='$m->icon'></i> <span>" . strtoupper($m->name)) . "</span></li>";
                            }
                        }
                        ?>
                            
                            

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        SISTEM INFORMASI PEMBAYARAN SEKOLAH SMP AL-FALAH AS-SALAM SIDOARJO
                    </h1>
                    <!--<ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Tables</a></li>
                        <li class="active">Data tables</li>
                    </ol>-->
                </section>


<?php
echo $contents;
?>



            </div><!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>Copyright &copy; 2016 UPN Jawa Timur.</strong> Teknik Informatika.
            </footer>

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div><!-- ./wrapper -->

        <!-- jQuery 2.1.4 -->
        <script src="<?php echo base_url() ?>template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo base_url() ?>template/bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url() ?>template/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url() ?>template/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url() ?>template/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url() ?>template/plugins/fastclick/fastclick.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url() ?>template/dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url() ?>template/dist/js/demo.js"></script>
        <!-- page script -->
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
            
            function searchMenu(){
                //$(li.menu_class[id^='"+cari_menu+"']").show();
                var cari_menu = $('#input_menu_search').val();
                cari_menu = cari_menu.toLowerCase();
                var li = "li[id^='"+cari_menu+"']";
               
               
                if(cari_menu=='')$("li.menu_class").show();
                else {
                    $("li.menu_class").hide();
                    $(li).show();
                }
            }
        </script>
    </body>
</html>
