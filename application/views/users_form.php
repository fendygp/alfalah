

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>

                    <h3 class='box-title'>USERS</h3>
                    <div class='box box-primary'>
                        <form action="<?php echo $action; ?>" method="post">
                            
                            <div class="form-group">
                                <label for="varchar">Username <?php echo form_error('username') ?></label>
                                <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Password <?php echo form_error('password') ?></label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
                            </div>
                          
                         
                            <div class="form-group">
                                <label for="varchar">Name <?php echo form_error('name') ?></label>
                                <input type="text" class="form-control" name="name" id="first_name" placeholder="First Name" value="<?php echo $name; ?>" />
                            </div>
                           
                           
                            <div class="form-group">
                                <label for="varchar">Phone <?php echo form_error('phone') ?></label>
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $phone; ?>" />
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                            <a href="<?php echo site_url('users') ?>" class="btn btn-default">Cancel</a>
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->

<script src="<?php echo base_url() ?>assets2/jquery-1.11.0.js"></script>

<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script src="<?php echo base_url(); ?>assets2/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets2/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets2/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>

<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
    $('.datepicker').datetimepicker({
        language: 'id',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
</script>
