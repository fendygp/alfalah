<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>LAPORAN</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th width="80px">No</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Jenis Bayar</th>
                <th>Tanggal bayar</th>
                <th>Jumlah Pemasukan</th>

            </tr><?php
            $a = 0;

            foreach ($hariandata as $harian) {
                ?>
                <tr>
                    <td><?php echo ++$a; ?></td>
                    <td><?php echo $harian->siswa_nis; ?></td>
                    <td><?php echo $harian->siswa_nama; ?></td>
                    <td><?php echo $harian->jenisbayar_ket ?></td>
                    <td><?php echo $harian->pembayaran_tanggal; ?></td>
                    <td><?php
                        if ($harian->pembayaran_jumlah == NULL) {
                            echo "0";
                        } else {
                            echo number_format($harian->pembayaran_jumlah,0);
                        }
                        ?></td>

                </tr>
            <?php } ?>
        </table>
    </body>
</html>