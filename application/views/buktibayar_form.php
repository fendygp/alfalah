

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>

                    <h3 class='box-title'>UPLOAD BUKTI BAYAR</h3>
                    <div class='box box-primary'>
                        <?php echo form_open_multipart($action); ?>

                        <div class="form-group">
                            <label for="varchar">Nama Siswa</label>
                            <input type="text" class="form-control" name="siswa_nama"  value="<?php echo $siswa_nama; ?>" readonly/>
                        </div>

                        <div class="form-group">
                            <label for="varchar">Upload gambar</label>
                            <input type="file" class="form-control" name="userfile" />
                        </div>

                        <div class="form-group">
                            <label for="varchar">Keterangan</label>
                            <select class="form-control"  name="keterangan">
                            <?php
                            foreach ($jenis_bayar as $jb) {

                                echo "<option value=" . $jb->jenisbayar_ket . ">$jb->jenisbayar_ket</option>";
                            }
                            ?>
                            </select>
                        </div>
                        <input type="hidden" name="wali_id" value="<?php echo $wali_id; ?>" />
                        <input type="hidden" name="siswa_id" value="<?php echo $siswa_id; ?>" />
                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="<?php echo site_url('upload_bukti') ?>" class="btn btn-default">Cancel</a>
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


