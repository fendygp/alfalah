<!-- file bootstrap css yang digunakan-->
<link href="<?php echo base_url(); ?>assets2/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets2/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>

                    <h3 class='box-title'>SISWA</h3>
                    <div class='box box-primary'>
                        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
                                <tr><td>NIS<?php echo form_error('siswa_nis') ?></td>
                                    <td><input type="text" class="form-control" name="siswa_nis" id="siswa_nis" placeholder="NIS" value="<?php echo $siswa_nis; ?>" />
                                    </td>
                                <tr><td>Kelas <?php echo form_error('sub_id') ?></td>
                                    <td>
                                        <select name="sub_id" class="form-control">

                                            <?php
                                            $queryku = "select s.sub_id, s.sub_nama, k.kelas_nama from subkelas as s inner join kelas as k on s.kelas_id=k.kelas_id";
                                            $subkelas = $this->db->query($queryku);
                                            foreach ($subkelas->result() as $m) {
                                                if ($sub_id == $m->sub_id) {

                                                    echo "<option value='" . $m->sub_id . "' selected>" . $m->kelas_nama . " " . $m->sub_nama . "</option>";
                                                } else {
                                                    echo "<option value='" . $m->sub_id . "'>" . $m->kelas_nama . " " . $m->sub_nama . "</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                <tr><td>Tahun Akademik <?php echo form_error('tahun_id') ?></td>
                                    <td><?php echo cmb_dinamis('tahun_id', 'tahun_akademik', 'tahun_ket', 'tahun_id', $tahun_id) ?>
                                    </td>
                                <tr><td>Nama Siswa <?php echo form_error('siswa_nama') ?></td>
                                    <td><input type="text" class="form-control" name="siswa_nama" id="siswa_nama" placeholder="Nama Siswa" value="<?php echo $siswa_nama; ?>" />
                                    </td>
                                <tr><td>Jenis Kelamin <?php echo form_error('siswa_jk') ?></td>
                                    <td>
                                        <select name="siswa_jk" class="form-control">
                                            <?php if ($siswa_jk == "Laki-Laki") {
                                                ?>
                                                <option value="Laki-Laki" selected>Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>  
                                                <?php
                                            } else {
                                                ?>
                                                <option value="Laki-Laki" >Laki-Laki</option>
                                                <option value="Perempuan" selected>Perempuan</option> 
                                            <?php } ?>


                                        </select>
                                    </td>
                                <tr><td>Tempat Lahir <?php echo form_error('siswa_tempatlahir') ?></td>
                                    <td><input type="text" class="form-control" name="siswa_tempatlahir" id="siswa_tempatlahir" placeholder="Tempat Lahir" value="<?php echo $siswa_tempatlahir; ?>" />
                                    </td>
                                <tr><td>Tanggal Lahir <?php echo form_error('siswa_tgllahir') ?></td>
                                    <td><input type="date" class="form-control datepicker" data-date-format="yyyy-mm-dd" name="siswa_tgllahir" id="siswa_tgllahir" placeholder="Tanggal Lahir" value="<?php echo $siswa_tgllahir; ?>" />
                                    </td>
                                <tr><td>Alamat <?php echo form_error('siswa_alamat') ?></td>
                                    <td><input type="text" class="form-control" name="siswa_alamat" id="siswa_alamat" placeholder="Alamat" value="<?php echo $siswa_alamat; ?>" />
                                    </td>
                                <tr><td>Nama Ayah <?php echo form_error('siswa_ayah') ?></td>
                                    <td><input type="text" class="form-control" name="siswa_ayah" id="siswa_ayah" placeholder="Nama Ayah" value="<?php echo $siswa_ayah; ?>" />
                                    </td>
                                <tr><td>Nama Ibu <?php echo form_error('siswa_ibu') ?></td>
                                    <td><input type="text" class="form-control" name="siswa_ibu" id="siswa_ibu" placeholder="Nama Ibu" value="<?php echo $siswa_ibu; ?>" />
                                    </td>
                                <tr><td>Pekerjaan Ayah <?php echo form_error('siswa_ayahkerja') ?></td>
                                    <td><input type="text" class="form-control" name="siswa_ayahkerja" id="siswa_ayahkerja" placeholder="Pekerjaan Ayah" value="<?php echo $siswa_ayahkerja; ?>" />
                                    </td>
                                <tr><td>Pekerjaan Ibu <?php echo form_error('siswa_ibukerja') ?></td>
                                    <td><input type="text" class="form-control" name="siswa_ibukerja" id="siswa_ibukerja" placeholder="Pekerjaan Ibu" value="<?php echo $siswa_ibukerja; ?>" />
                                    </td>
                                <tr><td>Alamat Orang Tua <?php echo form_error('siswa_alamatortu') ?></td>
                                    <td><input type="text" class="form-control" name="siswa_alamatortu" id="siswa_alamatortu" placeholder="Alamat Orang Tua" value="<?php echo $siswa_alamatortu; ?>" />
                                    </td>
                                <tr><td>Wali Username <?php echo form_error('user_wali') ?></td>
<td>
                                        <select class="form-control"  name="user_id">
                                            <?php
                                            foreach ($user_data as $ud) {

                                                if ($ud->group_id== "4") {
                                                    echo "<option value=" . $ud->id . " selected>$ud->username</option>";
                                                } else {

                                                    echo "<option value=" . $ud->id . " hidden>$ud->username</option>";
                                                }
                                            }
                                            ?>

                                        </select>

                                    </td>

                          
                                <input type="hidden" name="siswa_id" value="<?php echo $siswa_id; ?>" /> 
                                <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                                        <a href="<?php echo site_url('siswa') ?>" class="btn btn-default">Cancel</a></td></tr>
                                

                            </table></form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->

<script src="<?php echo base_url() ?>assets2/jquery-1.11.0.js"></script>

<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script src="<?php echo base_url(); ?>assets2/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets2/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets2/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>

<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
    $('.datepicker').datetimepicker({
        language: 'id',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
</script>