
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Data Persen dalam:
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="#" method="post">
                            
                            <div class="form-group">
                                <select class="form-control" name="jenis_lapor" disabled>
                                <option value="harian">Harian</option>
                                <option value="bulanan">Bulanan</option>
                                <option value="tahunan">Tahunan</option>
                            </select>
                            </div>

                            <button type="submit" class="btn btn-primary" disabled>Setup</button>
                            <a href="<?php echo site_url('Persenbayar') ?>" class="btn btn-default">Reset</a>
                        </form>

                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>

                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="<?php echo $action; ?>" method="post">

                            <div class="form-group">
                                <label>Bulan</label>
                                <select class="form-control" name="bulanawal">

                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Bulan</label>
                                <select class="form-control" name="bulanakhir">

                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>

                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Lihat</button>

                        </form>

                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->


    </div><!-- /.row -->
    <div class='row'>

        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
<!--                    <h3 class='box-title'>
                        <?php echo anchor(site_url('Laporankeu/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('Laporankeu/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('Laporankeu/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>-->
                </div><!-- /.box-header -->
                <div class='box-body'>

                    <div class="box-header with-border">
                        <h3 class="box-title">Jumlah Data Keuangan
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                        switch ($bulanawal) {
                            case 1:
                                $namabulan = "Januari";
                                break;
                            case 2:
                                $namabulan = "Februari";
                                break;
                            case 3:
                                $namabulan = "Maret";
                                break;
                            case 4:
                                $namabulan = "April";
                                break;
                            case 5:
                                $namabulan = "Mei";
                                break;
                            case 6:
                                $namabulan = "Juni";
                                break;
                            case 7:
                                $namabulan = "Juli";
                                break;
                            case 8:
                                $namabulan = "Agustus";
                                break;
                            case 9:
                                $namabulan = "September";
                                break;
                            case 10:
                                $namabulan = "Oktober";
                                break;
                            case 11:
                                $namabulan = "November";
                                break;
                            case 12:
                                $namabulan = "Desember";
                                break;
                        }

                        switch ($bulanakhir) {
                            case 1:
                                $namabulan2 = "Januari";
                                break;
                            case 2:
                                $namabulan2 = "Februari";
                                break;
                            case 3:
                                $namabulan2 = "Maret";
                                break;
                            case 4:
                                $namabulan2 = "April";
                                break;
                            case 5:
                                $namabulan2 = "Mei";
                                break;
                            case 6:
                                $namabulan2 = "Juni";
                                break;
                            case 7:
                                $namabulan2 = "Juli";
                                break;
                            case 8:
                                $namabulan2 = "Agustus";
                                break;
                            case 9:
                                $namabulan2 = "September";
                                break;
                            case 10:
                                $namabulan2 = "Oktober";
                                break;
                            case 11:
                                $namabulan2 = "November";
                                break;
                            case 12:
                                $namabulan2 = "Desember";
                                break;
                        }

                        echo $namabulan . " - " . $namabulan2;
                         $judul = "Jumlah Data Keuangan " .$namabulan . " - " . $namabulan2;
                        ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </h3>


                    </div>

                    <div class="box-body chart-responsive">
                        <div class="chart" id="bar-chart" style="height: 300px;"></div>
                    </div>
                    <!-- /.box-body -->
                </div>


            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div><!-- /.col -->


    <script src="<?php echo base_url('assets2/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
    <script src="<?php echo base_url('assets2/js/raphael-min.js') ?>"></script>

    <script src="<?php echo base_url('assets2/js/highcharts.js') ?>"></script>
    <script src="<?php echo base_url('assets2/js/modules/data.js') ?>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#mytable").dataTable();
        });
    </script>

    <script type="text/javascript">

        //RANDOM COLOR
        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }


        //HIGHCHART BAR
        $(document).ready(function () {
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'bar-chart',
                    type: 'column',
                    margin: [50, 50, 100, 80]
                },
                title: {
                    text: '<?php echo $judul; ?>'
                },
                xAxis: {
                    type: 'category'

                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total (Rupiah)'
                    }
                },
                legend: {
                    enabled: false
                },
                series: [{
                        name: 'Total',
                        data: [
<?php foreach ($lapordata as $lp) { ?>
                                {
                                    name: '<?php echo $lp->bulan."-".$lp->tahun; ?>',
                                    y: <?php echo $lp->total; ?>,
                                    color: getRandomColor()

                                },
<?php } ?>
                        ]
                    }]
            });


        });
    </script>
</section><!-- /.content -->

