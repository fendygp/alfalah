

<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Jenis Laporan
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="<?php echo $action; ?>" method="post">

                            <div class="form-group">
                                <select class="form-control" name="jenis_lapor" >
                                    <option value="harian">Harian</option>
                                    <option value="bulanan">Bulanan</option>
                                    <option value="tahunan">Tahunan</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Setup</button>
                            <a href="<?php echo site_url('laporankeu') ?>" class="btn btn-default">Reset</a>
                        </form>

                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->



    </div><!-- /.row -->
    <div class='row'>

        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>
                        <?php echo anchor(site_url('Laporankeu/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('Laporankeu/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('Laporankeu/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <table class="table table-bordered table-striped" id="mytable">
                        <thead>
                            <tr>
                                <th width="80px">No</th>
                                <th>Bulan</th>
                                <th>Tahun</th>
                                <th>Jumlah Pemasukan</th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $b = 0;

                            foreach ($lapordata as $a) {
                                ?>
                                <tr>
                                    <td><?php echo ++$b; ?></td>
                                    <td><?php echo $a->bulan; ?></td>
                                    <td><?php echo $a->tahun; ?></td>
                                    <td><?php
                                        if ($a->total == NULL) {
                                            echo "0";
                                        } else {
                                             echo number_format($a->total, 0);
                                        }
                                        ?></td>

                                </tr>
<?php } ?>

                        </tbody>
                    </table>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#mytable").dataTable();
                        });
                    </script>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

</section><!-- /.content -->

