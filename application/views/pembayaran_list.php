
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Data Siswa
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label>Nomor Induk Siswa (NIS)</label>
                                <input type="text" class="form-control" placeholder="Enter NIS">
                            </div>


                            <button type="submit" class="btn btn-primary">Search</button>
                            <a href="<?php echo site_url('pembayaran') ?>" class="btn btn-default">Reset</a>
                        </form>

                    </div>
                    <div class="col-xs-12" style="padding-top: 1em;">


                        <dl class="dl-horizontal">

                            <?php
                            foreach ($siswa_data as $siswa) {
                                $siswa_id = $siswa->siswa_id;
                                ?>
                                <dt>NIS</dt>
                                <dd><?php echo $siswa->siswa_nis; ?></dd>
                                <dt>Nama</dt>
                                <dd><?php echo $siswa->siswa_nama; ?></dd>
                                <dt>Kelas</dt>
                                <dd><?php echo $siswa->kelas_nama . " " . $siswa->sub_nama; ?></dd>

                                <dt>Tahun Ajaran</dt>
                                <dd><?php echo $siswa->tahun_ket; ?></dd>

                            <?php } ?>
                        </dl>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-xs-6'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Form Pembayaran
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <div class="col-xs-12">
                        <form action="<?php echo $action2; ?>" method="post">
                            <div class="form-group">
                                <label>Jenis Pembayaran</label>
                                <select class="form-control"  name="biayasekolah_id">
                                    <?php
                                    foreach ($yangdibayar_data as $jb) {
                                        $biayasudah = $this->db->query("
            SELECT 
                SUM(pembayaran_jumlah) as jumlahpembayaran
            FROM 
                pembayaran 
            WHERE 
                siswa_id = $siswa_id
            AND
                biayasekolah_id = $jb->biayasekolah_id
        ");
                                        foreach ($biayasudah->result() as $bs) {


                                            if ($bs->jumlahpembayaran == NULL || $bs->jumlahpembayaran < $jb->biayasekolah_jumlah) {
                                                echo "<option value=" . $jb->biayasekolah_id . ">$jb->jenisbayar_ket</option>";
                                            } elseif ($bs->jumlahpembayaran == $jb->biayasekolah_jumlah) {
                                                
                                            }
                                        }
                                    }
                                    ?>

                                </select>


                            </div>




                            <input type="hidden" class="form-control" name="siswa_id" value="<?php echo $siswa_id; ?>"/>


                            <input type="hidden" class="form-control" name="nis" value="<?php echo $siswa->siswa_nis; ?>"/>

                            <button type="submit" class="btn btn-primary" >Submit</button>

                        </form>

                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->


    <div class='row'>

        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>
                        
                        RINCIAN PEMBAYARAN
                        <?php echo anchor(site_url('biaya_sekolah/excel'), ' <i class="fa fa-file-excel-o"></i> Excel', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('biaya_sekolah/word'), '<i class="fa fa-file-word-o"></i> Word', 'class="btn btn-primary btn-sm"'); ?>
                        <?php echo anchor(site_url('biaya_sekolah/pdf'), '<i class="fa fa-file-pdf-o"></i> PDF', 'class="btn btn-primary btn-sm"'); ?></h3>
                </div>
                <div class='box-body'>
                    <table class="table table-bordered table-striped" id="mytable2">
                        <thead>
                            <tr>
                                <!--<th width="80px">No</th>-->
                                <th>Jenis pembayaran</th>
                                <th>Jumlah Tagihan</th>
                                <th>Sudah dibayar</th>
                                <th>Belum dibayar</th>
                                <th>Keteranngan</th>
                   
<!--                                <th>Action</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
//                            $a = 0;

                            foreach ($pembayaransiswa_data as $yangdibayar) {
                                ?>
                                <tr>
                                    <!--<td><?php echo ++$a; ?></td>-->
                                    <td><?php echo $yangdibayar->jenisbayar_ket; ?></td>
                                    <td><?php echo number_format($yangdibayar->biayasekolah_jumlah, 0); ?></td>
                                    <td><?php
                                        $biayasekolah_id = $yangdibayar->biayasekolah_id;
                                        $dibayar2 = $yangdibayar->bayar_jumlah;

                                        $hasil = $yangdibayar->biayasekolah_jumlah - $dibayar2;
                                        $pembayaran_id = $yangdibayar->pembayaran_id;
                                        echo number_format($yangdibayar->bayar_jumlah, 0);
                                        ?></td>

                                    <td><?php echo number_format($hasil, 0); ?></td>
                                    <td>
									<?php
                                    if ($hasil == 0) {
                                        echo 'Lunas';
                                    } else {
                                        echo 'Belum Lunas';
                                    }
                                    ?>
									</td>
                                   
    <!--                                    <td style="text-align:center" width="140px">
                                    <?php
                                    if ($dibayar2 == 0) {
                                        echo 'no action';
                                    } else {
                                        echo anchor(site_url('pembayaran/update/' . $pembayaran_id), '<i class="fa fa-pencil-square-o"></i>', array('title' => 'edit', 'class' => 'btn btn-danger btn-sm'));
                                        echo '  ';
                                        echo anchor(site_url('pembayaran/delete/' . $pembayaran_id), '<i class="fa fa-trash-o"></i>', 'title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                                    }
                                    ?>
                                    </td>-->
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#mytable2").dataTable();
                        });
                    </script>


                </div> 
            </div> 
        </div> 
    </div> 
    <div class='row'>

        <div class='col-xs-8'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>
                       DETAIL PEMBAYARAN</h3>
                </div>
                <div class='box-body'>
                    <table class="table table-bordered table-striped" id="mytable">
                        <thead>
                            <tr>
                                <!--<th width="80px">No</th>-->
                                <th>Jenis pembayaran</th>
                             
                                <th>Jumlah bayar</th>
                            
                                <th>Tanggal Bayar</th>
<!--                                <th>Action</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
//                            $a = 0;

                            foreach ($pembayaransiswa_data2 as $yangdibayar2) {
                                ?>
                                <tr>
                                    <!--<td><?php echo ++$a; ?></td>-->
                                    <td><?php echo $yangdibayar2->jenisbayar_ket; ?></td>
                                   
                                    <td><?php
                                        $biayasekolah_id = $yangdibayar2->biayasekolah_id;
                                        $dibayar2 = $yangdibayar2->pembayaran_jumlah;

                                    
                                        $pembayaran_id = $yangdibayar->pembayaran_id;
                                        echo number_format($dibayar2, 0);
                                        ?></td>

                                    
                                    <td><?php
                                        $tanggal = $yangdibayar2->pembayaran_tanggal;


                                        $tahun = '';
                                        $tgl = '';
                                        $bln = '';
                                        $thn = '';

                                        for ($i = 0; $i < strlen($tanggal); $i++) {
                                            if ($tanggal[$i] != "-") {
                                                $tahun = $tahun . $tanggal[$i];
                                            }
                                        }

                                        for ($i = 0; $i < 4; $i++) {
                                            $thn = $thn . $tahun[$i];
                                        }
                                        for ($i = 4; $i < 6; $i++) {
                                            $bln = $bln . $tahun[$i];
                                        }
                                        switch ($bln) {
                                            case 1:
                                                $namabulan = "Januari";
                                                break;
                                            case 2:
                                                $namabulan = "Februari";
                                                break;
                                            case 3:
                                                $namabulan = "Maret";
                                                break;
                                            case 4:
                                                $namabulan = "April";
                                                break;
                                            case 5:
                                                $namabulan = "Mei";
                                                break;
                                            case 6:
                                                $namabulan = "Juni";
                                                break;
                                            case 7:
                                                $namabulan = "Juli";
                                                break;
                                            case 8:
                                                $namabulan = "Agustus";
                                                break;
                                            case 9:
                                                $namabulan = "September";
                                                break;
                                            case 10:
                                                $namabulan = "Oktober";
                                                break;
                                            case 11:
                                                $namabulan = "November";
                                                break;
                                            case 12:
                                                $namabulan = "Desember";
                                                break;
                                        }
                                        for ($i = 6; $i < strlen($tahun); $i++) {
                                            $tgl = $tgl . $tahun[$i];
                                        }
                                        $tabel = $tgl . "&nbsp;" . $namabulan . "&nbsp;" . $thn;

                                        echo $tabel;
                                        ?></td>
    <!--                                    <td style="text-align:center" width="140px">
                                    <?php
                                    if ($dibayar2 == 0) {
                                        echo 'no action';
                                    } else {
                                        echo anchor(site_url('pembayaran/update/' . $pembayaran_id), '<i class="fa fa-pencil-square-o"></i>', array('title' => 'edit', 'class' => 'btn btn-danger btn-sm'));
                                        echo '  ';
                                        echo anchor(site_url('pembayaran/delete/' . $pembayaran_id), '<i class="fa fa-trash-o"></i>', 'title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                                    }
                                    ?>
                                    </td>-->
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#mytable").dataTable();
                        });
                    </script>


                </div> 
            </div> 
        </div> 
    </div>

</section><!-- /.content -->
