<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Info_siswa_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getSiswawali($user_id) {

        $this->db->select("siswa.siswa_id, siswa.siswa_nis, siswa.sub_id, siswa.tahun_id, siswa.siswa_nama, siswa.siswa_jk, siswa.siswa_tempatlahir, siswa.siswa_tgllahir, siswa.siswa_alamat, subkelas.sub_nama, kelas.kelas_nama, tahun_akademik.tahun_ket, users.username, users.id, kelas.kelas_id, tahun_akademik.tahun_id, siswa.user_wali");
        $this->db->join('users', 'siswa.user_wali = users.id');
        $this->db->join('subkelas', 'siswa.sub_id = subkelas.sub_id');
        $this->db->join('kelas', 'subkelas.kelas_id = kelas.kelas_id');
        $this->db->join('tahun_akademik', 'siswa.tahun_id = tahun_akademik.tahun_id');
        $this->db->where(array('siswa.user_wali' => $user_id));
        $query = $this->db->get('siswa');
        return $query->result();
    }

}
