<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function cek_user($username, $password) {

        $this->db->select('users.id as user_id, users.username, users.password, users.name, users.phone, groups.id as group_id');
        $this->db->from('users');
        $this->db->join('users_groups', 'users.id = users_groups.user_id');
        $this->db->join('groups', 'groups.id = users_groups.group_id', 'inner');
        $this->db->where(array('users.username' => $username, 'users.password' => $password));
        $query = $this->db->get();
        $query = $query->result_array();
        return $query;
    }

    function ambil_user($username) {
        $query = $this->db->get_where('users', array('username' => $username));
        $query = $query->result();
        if ($query) {
            return $query[0];
        }
    }

    function ambil_menu($group_id) {
        $this->db->select('*');
        $this->db->from('menu');
        $this->db->where(array('level' => $group_id));
        $query = $this->db->get();
        $query = $query->result();
        ;
        return $query;
    }

}
