<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upload_bukti_model extends CI_Model {

    public $table = 'bukti_bayar';
    public $id = 'id_laporan';
    public $order = 'DESC';

    function __construct() {
        parent::__construct();
    }

    function insert($data) {
        $this->db->insert('bukti_bayar', $data);
    }

    function get_all() {
        $this->db->select('bukti_bayar.id_laporan, bukti_bayar.wali_id, bukti_bayar.siswa_id, bukti_bayar.link_photo, bukti_bayar.keterangan, siswa.siswa_id, siswa.siswa_nama,siswa.siswa_nis, users.id, users.username, siswa.user_wali');
        $this->db->join('siswa', 'siswa.siswa_id = bukti_bayar.siswa_id');
        $this->db->join('users', 'users.id = siswa.user_wali');
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

}
