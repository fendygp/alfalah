<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporankeu_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function jumlahMasuktgl($tanggalawal, $tanggalakhir) {
        $sql = $this->db->query("
            SELECT 
                SUM(pembayaran_jumlah) as totalmasuk
            FROM 
                pembayaran             
            WHERE
                 DATE_FORMAT(pembayaran_tanggal,'%Y-%m-%d')
            BETWEEN
                '$tanggalawal'
            AND
                 '$tanggalakhir'
              ");
        return $sql->result();
    }

    function jumlahMasukbln($bulanawal, $bulanakhir) {
        $sql = $this->db->query("
            SELECT 
                SUM(pembayaran_jumlah) as totalmasuk
            FROM 
                pembayaran
            WHERE 
                DATE_FORMAT(pembayaran_tanggal,'%m')
            BETWEEN
                '$bulanawal'
            AND
                '$bulanakhir'
                ");
        return $sql->result();
    }

    public function jumlahMasukthn($tahunawal, $tahunakhir) {
        $sql = $this->db->query("
            SELECT 
                SUM(pembayaran_jumlah) as totalmasuk
            FROM 
                pembayaran
            WHERE 
                 DATE_FORMAT(pembayaran_tanggal,'%Y') 
            BETWEEN
            '$tahunawal'
            AND
            '$tahunakhir '
                 ");
        return $sql->result();
    }

    function getTahunAkademik() {
        $this->db->select("*");
        return $this->db->get('tahun_akademik')->result();
    }

    function jumlahKredit() {
        $sql = $this->db->query("
            SELECT 
                SUM(biayasekolah_jumlah) as totalkredit
            FROM 
                biaya_sekolah
          ");
        return $sql->result();
    }

    function jumlahKreditthn($tahun) {
        $sql = $this->db->query("
            SELECT 
                SUM(biayasekolah_jumlah) as totalkredit
            FROM 
                biaya_sekolah
            WHERE
                 tahunakademik_id = $tahun
          ");
        return $sql->result();
    }

    function getHarian($tanggalawal, $tanggalakhir) {
        $sql = $this->db->query("
            SELECT 
                pembayaran.* , biaya_sekolah.*, jenisbayar.*, siswa.*
            FROM 
                pembayaran
            LEFT JOIN biaya_sekolah ON biaya_sekolah.biayasekolah_id = pembayaran.biayasekolah_id
            LEFT JOIN jenisbayar ON biaya_sekolah.jenisbayar_id = jenisbayar.jenisbayar_id
            LEFT JOIN siswa ON siswa.siswa_id = pembayaran.siswa_id
            WHERE
                 DATE_FORMAT(pembayaran_tanggal,'%Y-%m-%d')
            BETWEEN
                '$tanggalawal'
            AND
                 '$tanggalakhir'
              ");
        return $sql->result();
    }

    function getBulanan($bulanawal, $bulanakhir) {
        $sql = $this->db->query("
            SELECT 
                pembayaran.* , biaya_sekolah.*, jenisbayar.*, siswa.*
            FROM 
                pembayaran
            LEFT JOIN biaya_sekolah ON biaya_sekolah.biayasekolah_id = pembayaran.biayasekolah_id
            LEFT JOIN jenisbayar ON biaya_sekolah.jenisbayar_id = jenisbayar.jenisbayar_id
            LEFT JOIN siswa ON siswa.siswa_id = pembayaran.siswa_id
            WHERE
                 DATE_FORMAT(pembayaran_tanggal,'%m')
            BETWEEN
                '$bulanawal'
            AND
                 '$bulanakhir'
              ");
        return $sql->result();
    }

    function getTahunan($tahunawal, $tahunakhir) {
        $sql = $this->db->query("
            SELECT 
                pembayaran.* , biaya_sekolah.*, jenisbayar.*, siswa.*
            FROM 
                pembayaran
            LEFT JOIN biaya_sekolah ON biaya_sekolah.biayasekolah_id = pembayaran.biayasekolah_id
            LEFT JOIN jenisbayar ON biaya_sekolah.jenisbayar_id = jenisbayar.jenisbayar_id
            LEFT JOIN siswa ON siswa.siswa_id = pembayaran.siswa_id
            WHERE
                 DATE_FORMAT(pembayaran_tanggal,'%Y')
            BETWEEN
                '$tahunawal'
            AND
                 '$tahunakhir'
              ");
        return $sql->result();
    }

    //-------------------------------revisi------------------------------------------------

    function persenPerhari($tanggalawal,$tanggalakhir) {
        $sql = $this->db->query("
            SELECT DATE(`pembayaran_tanggal`) as tanggal, SUM(`pembayaran_jumlah`) as total
            FROM pembayaran
            
            WHERE
                 DATE_FORMAT(pembayaran_tanggal,'%Y-%m-%d')
            BETWEEN
                '$tanggalawal'
            AND
                '$tanggalakhir'
           GROUP BY DATE(`pembayaran_tanggal`)
"
        );
        return $sql->result();
    }

    function persenPerbulan($bulanawal,$bulanakhir) {
        $sql = $this->db->query("
              SELECT MONTHNAME(`pembayaran_tanggal`) as bulan,YEAR(`pembayaran_tanggal`) as tahun, SUM(`pembayaran_jumlah`) as total
            FROM pembayaran
           
             WHERE
                 DATE_FORMAT(pembayaran_tanggal,'%m')
            BETWEEN
                '$bulanawal'
            AND
                 '$bulanakhir'
             GROUP BY YEAR(`pembayaran_tanggal`), MONTH(`pembayaran_tanggal`)
              ");

        return $sql->result();
    }

    function persenPertahun($tahunawal,$tahunakhir) {
        $sql = $this->db->query("
              SELECT YEAR(`pembayaran_tanggal`) as tahun, SUM(`pembayaran_jumlah`) as total
            FROM pembayaran
            WHERE
                 DATE_FORMAT(pembayaran_tanggal,'%Y')
            BETWEEN
                '$tahunawal'
            AND
                 '$tahunakhir'
            GROUP BY YEAR(`pembayaran_tanggal`)
"
        );
        return $sql->result();
    }

}
