<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembayaran_model extends CI_Model {

    public $table = 'pembayaran';
    public $id = 'pembayaran_id';
    public $order = 'DESC';

    function __construct() {
        parent::__construct();
    }

    // get all
    function get_all() {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id) {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('pembayaran_id', $q);
        $this->db->or_like('siswa_id', $q);
        $this->db->or_like('biayasekolah_id', $q);
        $this->db->or_like('pembayaran_tanggal', $q);
        $this->db->or_like('pembayaran_jumlah', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('pembayaran_id', $q);
        $this->db->or_like('siswa_id', $q);
        $this->db->or_like('biayasekolah_id', $q);
        $this->db->or_like('pembayaran_tanggal', $q);
        $this->db->or_like('pembayaran_jumlah', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data) {
        return $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function getJenisbayar() {
        $this->db->select("*");
        return $this->db->get('jenisbayar')->result();
    }

    function getSiswa($nis) {
        $this->db->select('siswa.siswa_id, siswa.siswa_nis, siswa.siswa_nama, siswa.sub_id, siswa.tahun_id, subkelas.sub_nama, subkelas.kelas_id, subkelas.sub_id, tahun_akademik.tahun_ket, tahun_akademik.tahun_id, kelas.kelas_id, kelas.kelas_nama');
        $this->db->join('subkelas', 'siswa.sub_id = subkelas.sub_id');
        $this->db->join('kelas', 'subkelas.kelas_id = kelas.kelas_id');
        $this->db->join('tahun_akademik', 'siswa.tahun_id = tahun_akademik.tahun_id');
        return $this->db->get_where('siswa', array('siswa_nis' => $nis))->result();
    }

    function getPembayaransiswa($siswa_id) {
        $this->db->select('pembayaran.pembayaran_id, pembayaran.siswa_id, pembayaran.biayasekolah_id, pembayaran.pembayaran_tanggal, sum(pembayaran.pembayaran_jumlah) as bayar_jumlah, pembayaran.pembayaran_jumlah, pembayaran.sisa,biaya_sekolah.jenisbayar_id, biaya_sekolah.biayasekolah_jumlah, jenisbayar.jenisbayar_ket, siswa.siswa_id, siswa.siswa_nis, siswa.siswa_nama, pembayaran.statusbayar');
        $this->db->join('biaya_sekolah', 'biaya_sekolah.biayasekolah_id = pembayaran.biayasekolah_id');
        $this->db->join('jenisbayar', 'biaya_sekolah.jenisbayar_id = jenisbayar.jenisbayar_id');
       
        $this->db->join('siswa', 'siswa.siswa_id = pembayaran.siswa_id');
        $this->db->group_by('jenisbayar.jenisbayar_ket');
        return $this->db->get_where('pembayaran', array('pembayaran.siswa_id' => $siswa_id))->result();
    }
	
    function getPembayaransiswa2($siswa_id) {
        $this->db->select('pembayaran.pembayaran_id, pembayaran.siswa_id, pembayaran.biayasekolah_id, pembayaran.pembayaran_tanggal, pembayaran.pembayaran_jumlah, pembayaran.sisa,biaya_sekolah.jenisbayar_id, biaya_sekolah.biayasekolah_jumlah, jenisbayar.jenisbayar_ket, siswa.siswa_id, siswa.siswa_nis, siswa.siswa_nama, pembayaran.statusbayar');
        $this->db->join('biaya_sekolah', 'biaya_sekolah.biayasekolah_id = pembayaran.biayasekolah_id');
        $this->db->join('jenisbayar', 'biaya_sekolah.jenisbayar_id = jenisbayar.jenisbayar_id');
       
        $this->db->join('siswa', 'siswa.siswa_id = pembayaran.siswa_id');
        $this->db->order_by('pembayaran.pembayaran_tanggal', 'ASC');
        return $this->db->get_where('pembayaran', array('pembayaran.siswa_id' => $siswa_id))->result();
    }

    function getYangharusdibayar($tahun_id, $kelas_id) {
        $this->db->select('biaya_sekolah.biayasekolah_id, biaya_sekolah.biayasekolah_jumlah, biaya_sekolah.jenisbayar_id, biaya_sekolah.tahunakademik_id, biaya_sekolah.jenisbayar_id, jenisbayar.jenisbayar_ket, biaya_sekolah.biayasekolah_id');
        $this->db->join('jenisbayar', 'biaya_sekolah.jenisbayar_id = jenisbayar.jenisbayar_id');
        return $this->db->get_where('biaya_sekolah', array('biaya_sekolah.tahunakademik_id' => $tahun_id, 'biaya_sekolah.kelas_id' => $kelas_id))->result();
    }

    function getBiaya($biayasekolah_id) {
        $this->db->select('biaya_sekolah.biayasekolah_id,biaya_sekolah.biayasekolah_jumlah, biaya_sekolah.jenisbayar_id, biaya_sekolah.tahunakademik_id, biaya_sekolah.jenisbayar_id, jenisbayar.jenisbayar_ket, biaya_sekolah.biayasekolah_id');
        $this->db->join('jenisbayar', 'biaya_sekolah.jenisbayar_id = jenisbayar.jenisbayar_id');
        return $this->db->get_where('biaya_sekolah', array('biaya_sekolah.biayasekolah_id' => $biayasekolah_id))->result();
    }

    function jumlahperjenisbayar($siswa_id, $biayasekolah_id) {

        $sql = $this->db->query("
            SELECT 
                SUM(pembayaran_jumlah) as jumlahpembayaran
            FROM 
                pembayaran 
            WHERE 
                siswa_id = $siswa_id
            AND
                biayasekolah_id = $biayasekolah_id
        ");
        return $sql->result();
    }

    function insert_status($data) {
        return $this->db->insert('status_bayar', $data);
    }
    
    function insert_notif($data) {
        $this->db->insert('notifikasi', $data);

        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    function countnotif($siswa_id) {
        $sql = $this->db->query("
            SELECT 
                count(*) as jumlahnotif
            FROM 
                notifikasi 
            WHERE 
                siswa_id = $siswa_id
        ");
        return $sql->result();
    }
    
    function getnotif($siswa_id){
        $this->db->select('notifikasi.*, biaya_sekolah.jenisbayar_id,jenisbayar.jenisbayar_ket');
        $this->db->join('biaya_sekolah', 'biaya_sekolah.biayasekolah_id = notifikasi.biayasekolah_id');
        $this->db->join('jenisbayar', 'jenisbayar.jenisbayar_id = biaya_sekolah.jenisbayar_id');
        return $this->db->get_where('notifikasi', array('notifikasi.siswa_id' => $siswa_id))->result();
    }
    
    function deletenotif($siswa_id){
        $this->db->where('notifikasi.siswa_id', $siswa_id);
        return $this->db->delete('notifikasi');
    }

}

/* End of file Pembayaran_model.php */
/* Location: ./application/models/Pembayaran_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-07-22 05:30:14 */
/* http://harviacode.com */